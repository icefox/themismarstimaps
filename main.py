#!/usr/bin/env python
# This is designed to run on python 2.6.
# 
# Okay, so this script will:
# 1) Read in a csv file and split it into fields
# 2) For each feature location in the CSV file:
#   * Create a working directory
#   * Find the optimal Ls to look at
#   * Query the THEMIS stamp server to find appropriate images
#   * Create a pairs file specifying pairs
#   * Create a file_list file containing all the images
#   * Set environment variables, and run the ~/src/project_ir_isis3.sh program to get the files
# 3) For each image pair, run temp2depth.py to produce output files.
# Easy peasy, right?
#
# It has some slightly arcane options to ensure you can do repeated runs without having to
# re-do a lot of the expensive calculation, instead just using the images and image pair lists
# it already has.
#
# It would also be nice if we could tell it to run and only use the images currently in
# the directories, instead of trying to get more.
#
# It is not really built in a modular fashion; it is not meant to be re-used as a library,
# it is a front-end program that glues together a bunch of other stuff.

import csv
import sys
import os
import shutil
import subprocess
import time

from osgeo import gdal
from osgeo import ogr
from osgeo import osr
from osgeo import gdal_array
from osgeo import gdalconst as gdc

import numpy as np

import krc
import themisStamp
import temp2depth

# Sooper custom modded version lets you specify clon without
# modifying the file.
PROJECTPROG = "/mars/u/sheath/src/project_ir_isis3.sh"
IMGDEPTH = "/mars/u/sheath/src/imgdepth.py"
DUSTMAPFILE = "/mars/u/sheath/dat/dustmap/dustmap-final.tif"

# Seconds to wait between dispatching cluster jobs
KICKOFFDELAY = 0.25
# Seconds to wait between polling the cluster to see if it's done
CLUSTERPOLLDELAY = 60
# Cluster username to poll for
CLUSTERUSER = "sheath"
# Max jobs to start on the cluster
CLUSTERJOBS = 500

NOTESLOG = 'run.log'

usage = """
%s <filename>
    where <filename> is a csv file with your image list in it.
""" % sys.argv[0]

def yesno():
    """Prompts for input, returns true or false."""
    res = raw_input().lower()
    return (res != '') and (res[0] == 'y')

WORKDIR = "working"

noteslogFile = 0

def setupLogs():
    """Initialize log files for the current working dir."""
    global noteslogFile
    fname = os.path.join(WORKDIR, NOTESLOG)
    noteslogFile = open(fname, 'w')


def log(message):
    """Writes the message to the log file."""
    noteslogFile.write(str(message))
    noteslogFile.write('\n')
    noteslogFile.flush()

def setupWorkDir():
    """Creates the current working directory, asks for confirmation to re-create it if it
exists."""
    try:
        os.makedirs(WORKDIR)
    except OSError:
        print("Work directory exists, delete and recreate?  If you answer no, it will still go through and reprocess old directories (though it won't project images more than once). [y/N] ")
        if yesno():
            shutil.rmtree(WORKDIR)
            setupWorkDir()

def imageDirName(n, image):
    """Returns a path string to the given image directory inside the working dir."""
    return os.path.join(WORKDIR, "{0:03d}-{1}".format(n, image))

def imageDirExists(imagedir):
    """Checks whether the given image directory exists."""
    return os.path.isdir(imagedir)

def makeImageDir(imagedir):
    """Create the given image dir if it doesn't exist."""
    os.makedirs(imagedir)
    
def openCSVFile(name):
    """Returns a csv.DictReader for the given file name."""
    try:
        f = open(name, 'r')
    except IOError:
        raise RuntimeError("CSV file does not exist: " + name)
    reader = csv.DictReader(f)    
    return reader

# XXX: Verify that this has intended behavior!
# Bandfield & Feldman 2008 limited the range to 85.5-220.5
# for northern hemisphere, and 265.5-40.5 for southern hemisphere!
def findBestLs(lat, lon, ti, tau, albedo, elevation):
    """Runs two KRC simulations with layer change at 5 and 20 layers,
and finds the Ls's where the differences are greatest."""
    #log("Finding best LS with lat {0} lon {1} ti {2} 
    # KRC takes elevation in km, while we use meters.
    topLayer = krc.runKRC(lat, lon, ti=ti, tau=tau, alb=albedo, elev=(elevation / 1000.0), layer=5)
    bottomLayer = krc.runKRC(lat, lon, ti=ti, tau=tau, alb=albedo, elev=(elevation / 1000.0), layer=20)
    # We ignore temps colder than 160, since CO2 freezes in the 148-155 range.
    minTemp = 160
    topTemps = topLayer['tk'][:,5]
    bottomTemps = bottomLayer['tk'][:,5]
    diffTemps = topTemps - bottomTemps
    for i in range(len(topTemps)):
        if topTemps[i] < minTemp:
            diffTemps[i] = 0
        if bottomTemps[i] < minTemp:
            diffTemps[i] = 0
    maxDiffTime = np.argmax(diffTemps)
    minDiffTime = np.argmin(diffTemps)
    maxLs = (maxDiffTime / 80.0) * 360
    minLs = (minDiffTime / 80.0) * 360
    #return (topTemps, bottomTemps, diffTemps, maxLs, minLs)
    return (maxLs, minLs)

def findBestLsNew(lat, lon, ti, tau, albedo, elevation):
    pass


# XXX: This would be better if it did not re-load
# the file each time it was called.
def findDust(lat, lon):
    """Loads DUSTMAPFILE and grabs the value at the given lat and lon."""
    gdata = gdal.Open(DUSTMAPFILE, gdc.GA_ReadOnly)
    band = gdata.GetRasterBand(1).ReadAsArray()
    gdata = None
    ppd = 2
    # The data appears to be in [row,column] order.
    xoffset = int(lon * ppd) + 1
    yoffset = int((90 - lat) * ppd) + 1

    dustVal = float(band[yoffset,xoffset])
    # Correct absorbtion value to one that includes scattering
    # And also is valid for visible wavelengths.
    # A la Fergason 2006
    dustVal = dustVal * 2.0
    return dustVal

def writeImagePairs(workdir, pairs):
    """Writes out a file containing the pairs of images to run simulations against."""
    filename = os.path.join(workdir, "pairs")
    f = open(filename, 'w')
    for (i,j) in pairs:
        f.write("%s,%s\n" % (i,j))
    f.close()
    return filename

def permuteImages(imgs1, imgs2):
    """Takes two lists of images, returns a list of tuples containing all possible combinations.
At least ones where imgs1 is before imgs2."""
    imgpairs = set()
    for x in imgs1:
        for y in imgs2:
            img1,_ = x
            img2,_ = y
            # I love python sets.
            # ...though honestly, these sets are not large so lists would be just as fast.
            # Still!  It makes more semantic sense this way.  We can't have duplicates.
            if img1 != img2 and not (img2, img1) in imgpairs:
                imgpairs.add((img1, img2))
    return list(imgpairs)

def getImageIDs(pairs1, pairs2):
    """Takes two lists of imagepairs, returns a list of the unique ones."""
    ids = set()
    for (idnum, _) in pairs1:
        ids.add(idnum)
    for (idnum, _) in pairs2:
        ids.add(idnum)

    # Skip if we only have images from one Ls
    if(len(ids) < 2):
        return []
    return list(ids)

def writeFileList(workdir, imgids):
    """Takes a list of image ID's and writes them out to a file to be fetched/projected
with PROJECTPROG"""
    filename = os.path.join(workdir, "file_list")
    f = open(filename, 'w')
    for i in imgids:
        f.write("{0}\n".format(i))
    f.close()
    return filename

def countRunningClusterJobs():
    """Returns the number of cluster jobs currently running for CLUSTERUSER.
Crude, but it works."""
    proc = subprocess.Popen(["qstat"], stdout=subprocess.PIPE)
    # Wait for it to complete, and get return value.
    retval = proc.wait()
    result = proc.stdout.read()
    if retval != 0:
        log(result)
        log("qstat bombed out for some reason!")
    lines = result.split("\n")
    count = 0
    for line in lines:
        if CLUSTERUSER in line:
            count += 1
    return count


def startProjectionJob(workdir, lon):
    """Invokes PROJECTPROG on the given work dir and longitude.
project_ir_isis3.sh at least is smart enough to not re-create images
that already exist."""
    here = os.getcwd()
    os.chdir(workdir)
    os.environ['clon'] = lon
    os.system(PROJECTPROG)
    #os.system("pwd")
    #os.system("echo $clon")
    os.chdir(here)
    time.sleep(KICKOFFDELAY)

def isisTrim(workdir, lat, lon):
    """Kicks off cluster jobs to crop every .cub image in the given working directory
to the given lat and lon +- 1 degree.
ISIS3 is wonderful.
Well, semi-wonderful."""
    log("Trimming " + workdir)
    lat = float(lat)
    lon = float(lon)
    radius = 1.0
    minlat = (lat - radius)
    maxlat = (lat + radius)
    minlon = (lon - radius) % 360
    maxlon = (lon + radius) % 360
    files = os.listdir(workdir)
    if minlat > maxlat:
        tmp = minlat
        minlat = maxlat
        maxlat = tmp
    if minlon > maxlon:
        tmp = minlon
        minlon = maxlon
        maxlon = tmp
        
    workdir = os.path.join(os.getcwd(), workdir)
    for fil in files:
        base, ext = os.path.splitext(fil)
        if len(base) != 9 or ext != '.cub':
            # Skip non-cube files
            # which are not our base projected images
            continue
        srcfile = os.path.join(workdir, fil)
        destfile = os.path.join(workdir, base + '-trimmed' + ext)
        # Skip files which already exist
        if os.path.exists(destfile):
            log("Skipping {0} since it already exists".format(destfile))
            continue

        #bakfile = os.path.join(workdir, base + '-untrimmed' + ext)
        #file = os.path.join(workdir, file)
        #destfile = os.path.join(workdir, destfile)
        #commandline = "source /themis/lib/mosaic/setupisis3.bashrc; /mars/common/isis3/x86_64_Linux/isis/bin/maptrim mode=both from={src} to={dst} minlat={minlat} maxlat={maxlat} minlon={minlon} maxlon={maxlon};"
        commandline = "/mars/common/isis3/x86_64_Linux/isis/bin/maptrim mode=both from={src} to={dst} minlat={minlat} maxlat={maxlat} minlon={minlon} maxlon={maxlon}"
#mv -f {src} {bak}
#mv -f {dst} {src}
        log(commandline)
        command = commandline.format(
            src=srcfile, dst=destfile, #bak=bakfile, 
            minlat=minlat, maxlat=maxlat, minlon=minlon, maxlon=maxlon)
        # Well we CAN'T DO THIS since maptrim is retarded and uses the same
        # temporary file all the time even for multiple processes.
        # We could solve this by wrapping maptrim in a shell flock command,
        # since that appears to work on whatever NFS the Mars Space Flight 
        # Facility uses... but, it's easier to just suck it and do it all
        # locally.
        #spawnClusterJob(workdir, 'maptrim', command)

        spawnNonClusterJob(workdir, command.split(' '))
        # We delete the original file since it takes up a lot of
        # space and we have yet to need it.  Make sure to remove
        # this if we do end up needing it someday, or if we end
        # up farming jobs out to the cluster afterall!  Using
        # the same srcfile and destfile name can effectively
        # overwrite the source file with the trimmed version too.
        # ...HOWEVER, since we rename the source file then the ISIS3
        # projection script re-projects the thing on each run!  Aiee!
        #os.remove(srcfile)
        

def readImagePairs(workdir):
    """Turns an image pair file into a list of tuples."""
    pairfile = os.path.join(workdir, 'pairs')
    f = open(pairfile, 'r')
    lines = f.readlines()
    f.close()
    accm = []
    for x in lines:
        s = x.split(',')
        img1 = s[0].strip()
        img2 = s[1].strip()
        accm.append((img1, img2))
    return accm

def spawnClusterJob(workdir, commandName, commandline, envvars={}, limits=''):
    """Spawns a cluster job with the given command line and such in the given
working directory.  Will wait if there are too many jobs already running.
Will also wait a short delay to make sure we don't flood the server with jobs.
"""
    numjobs = countRunningClusterJobs()
    while numjobs > CLUSTERJOBS:
        log("Waiting for cluster: %s/%s jobs running" % (numjobs, CLUSTERJOBS))
        time.sleep(CLUSTERPOLLDELAY)
        numjobs = countRunningClusterJobs()

    here = os.getcwd()
    #print "Before", os.getcwd()
    os.chdir(workdir)
    #print "After", os.getcwd()
    #commandName = commandline.split(' ')[0]
    qsubCommand = ["qsub", "-d", os.getcwd(), "-u", CLUSTERUSER, "-q", "low", '-N', commandName, '-l', limits]
    log(os.getcwd())
    log(' '.join(qsubCommand))
    log(commandline)
    proc = subprocess.Popen(qsubCommand, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    proc.stdin.write(commandline)
    proc.stdin.close()

    retval = proc.wait()
    log(proc.stdout.read())
    if retval != 0:
        log(proc.stderr.read())
        log("qsub bombed out for some reason!")
    else:
        log("Done!")
    os.chdir(here)
    time.sleep(KICKOFFDELAY)

def spawnNonClusterJob(workdir, commandline):
    """Sorta the same as spawnClusterJob, but does it locally.
Unfortunately this also means you can't do walltime limits or such.
Oh well."""
    log(commandline)

    here = os.getcwd()
    os.chdir(workdir)
    proc = subprocess.Popen(commandline, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    retval = proc.wait()
    log(proc.stdout.read())
    if retval != 0:
        log(proc.stderr.read())
        log("Local job bombed out for some reason!")
    else:
        log("Done!")
    os.chdir(here)


def runSimulation(workdir, img1, img2, ti, tau, albedo, elevation, lat, lon, cluster = True):
    """Runs a simulation job on the given location and images."""
    img1file = img1 + '-trimmed.cub'
    img2file = img2 + '-trimmed.cub'
    command = [IMGDEPTH, img1file, img2file, ti, tau, albedo, elevation, lat, lon]
    #print ' '.join(command)
    if cluster:
        spawnClusterJob(workdir, 'imgdepth', ' '.join(command), limits='walltime=00:30:00')
    else:
        spawnNonClusterJob(workdir, command)

###                                                                               ###
# Below here are actual top-level functions that start putting everything together. #
###                                                                               ###

def getLat(line):
    lat = ""
    if line.has_key("latitude"): lat = line["latitude"]
    else: lat = line["lat"]
    if lat == "":
        raise KeyError("latitude")
    return lat

def getLon(line):
    lon = ""
    if line.has_key("longitude"): lon = line["longitude"]
    else: lon = line["lon"]
    if lon == "":
        raise KeyError("longitude")
    return lon

def setupImageDirs(infile):
    """
This function goes through a CSV file and for each feature listed,
it gets figures out image pairs, and if project=True it kicks off
cluster jobs to acquire and project the THEMIS images it needs.
It also sets up the directory tree.
So it basically does everything except actually run the simulations;
that happens later, so that we can acquire all the projected images at once,
and then do the actual analysis of each one in turn.

The way to parallize this is to have each iteration of the for loop
spawn in its own thread, and then just keep N threads working at once.
One problem with this would be log file output.
It's really just not worth the trouble.
"""
    csvf = openCSVFile(infile)
    n = 0
    for line in csvf:
        ctxImageName = line["Parent image ID:string"]

        lat = getLat(line)
        lon = getLon(line)

        log("Working on image {0} ({1}) at lon {2}, lat {3}".format(
                n, ctxImageName, lon, lat))
        n += 1
        imagedir = imageDirName(n, ctxImageName)
        if imageDirExists(imagedir):
            # If the dir already exists, skip it.
            pass
            #log("Skipping...")
            #continue
        else:
            makeImageDir(imagedir)
   
        ti = line["Avg TES TI:double"]
        # This is just for finding the best Ls, not actually doing
        # the simulation, so having slightly wrong dust values here
        # doesn't matter.
        # Tau has roughly the same effect on temperature in all seasons.
        tau = 0.3
        albedo = line["Avg TES albedo:double"]
        elevation = line["Elevation:double"]
        
        pairfilename = os.path.join(imagedir, "pairs")
        filelistname = os.path.join(imagedir, "file_list")

        # Might want to set this to false when resuming a run,
        # since it takes a little while.
        recreateFiles = True
        if (not os.path.isfile(pairfilename)) or \
           (not os.path.isfile(filelistname)) or \
           recreateFiles:
            print "Looking for images..."
            ls1, ls2 = findBestLs(float(lat), float(lon), float(ti), \
                                      float(tau), float(albedo), float(elevation))
            images1 = themisStamp.getImagesAt(float(lat), float(lon), float(ls1), 45.0)
            images2 = themisStamp.getImagesAt(float(lat), float(lon), float(ls2), 45.0)
            log("Images in right area ({0}): {1}, {2}".format(
                    len(images1) + len(images2), images1, images2))

            def imageIsNotFrozen(x):
                name, ls = x
                return temp2depth.imageIsNotFrozen(name)
            
            images1 = filter(imageIsNotFrozen, images1)
            images2 = filter(imageIsNotFrozen, images2)
            log("Images in right area and warm enough ({0}): {1}, {2}".format(
                        len(images1) + len(images2), images1, images2))

            combinations = permuteImages(images1, images2)
            writeImagePairs(imagedir, combinations)
            
            images = getImageIDs(images1, images2)
            writeFileList(imagedir, images)
        else:
            log("Not bothering to recreate " + pairfilename + " and " + filelistname)
            log("To change this behavior, alter the recreateFiles variable in step1.py")
        

        startProjectionJob(imagedir, lon)
        log('')
        sys.stdout.flush()
        sys.stderr.flush()

def waitForClusterJobsToFinish():
    """Waits until all cluster jobs for CLUSTERUSER are done.
Is not smart enough to track individual jobs, so if you spawn more while
this is waiting it will wait for those to finish too!
"""
    currentJobs = countRunningClusterJobs()
    while currentJobs > 0:
        now = time.strftime("%H:%M:%S", time.localtime())
        log("%s Running jobs: %s" % (now, currentJobs))
        sys.stdout.flush()
        sys.stderr.flush()
        time.sleep(CLUSTERPOLLDELAY)
        currentJobs = countRunningClusterJobs()

def trimImages(infile):
    """Does isisTrim() to all image directories for all locations specified by infile."""
    csvf = openCSVFile(infile)
    n = 0
    for line in csvf:
        n += 1
        ctxImageName = line["Parent image ID:string"]
        log("Working on image %s: %s" % (n, ctxImageName))
        imagedir = imageDirName(n, ctxImageName)
        lat = getLat(line)
        lon = getLon(line)
        isisTrim(imagedir, lat, lon)

def runSimulations(infile):
    """
This function walks down the image tree set up in
setupImageDirs, and runs the actual simulation for
each image pair
"""
    csvf = openCSVFile(infile)
    n = 0
    for line in csvf:
        n += 1
        ctxImageName = line["Parent image ID:string"]
        log("Working on image %s: %s" % (n, ctxImageName))
        imagedir = imageDirName(n, ctxImageName)
        lat = getLat(line)
        lon = getLon(line)

        ti = line["Avg TES TI:double"]
        tau = findDust(float(lat), float(lon))
        print("DUST: {0}".format(tau))
        albedo = line["Avg TES albedo:double"]
        elevation = line["Elevation:double"]
        pairs = readImagePairs(imagedir)
        log(pairs)
        for (img1,img2) in pairs:
            #runSimulation(imagedir, img1, img2, ti, str(tau), albedo, elevation, lat, lon, cluster=False)
            runSimulation(imagedir, img1, img2, ti, str(tau), albedo, elevation, lat, lon, cluster=True)
            sys.stdout.flush()
        log('')


def main():
    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)
    #print("Did you remember to run isis3setup?!!?!?!?!?!?")

    base, _ = os.path.splitext(sys.argv[1])
    global WORKDIR
    WORKDIR = base
    setupWorkDir()

    setupLogs()
    infile = sys.argv[1]
    setupImageDirs(infile)
    waitForClusterJobsToFinish()
    trimImages(infile)
    waitForClusterJobsToFinish()
    runSimulations(infile)
    waitForClusterJobsToFinish()
    log("Finally, finally finished.  Except for the bugs.  And the science.")

if __name__ == '__main__':
    main()
