#!/usr/bin/env python
import sys
from osgeo import gdal
from osgeo import gdalconst as gdc

def loadPDS(filename):
    f = gdal.Open(filename, gdc.GA_ReadOnly)
    return f

def dims(gdal):
    return (gdal.RasterXSize, gdal.RasterYSize)

# See http://www.gdal.org/gdal_datamodel.html
def overlay(gdal1, gdal2):
    gt1 = gdal1.GetGeoTransform()
    gt2 = gdal2.GetGeoTransform()

    if gt1 == None or gt2 == None:
        raise RuntimeError("gdal image does not have a geotransform!")
    if gt1[1] != gt2[1] or gt1[5] != gt2[5]:
        raise RuntimeError("gdal images do not have same resolution!")

    xdiff = gt2[0] - gt1[0]
    ydiff = gt2[3] - gt1[3]
    print "Differences between image offsets: ", xdiff, ydiff

    dims1 = dims(gdal1)
    dims2 = dims(gdal2)
    print "Image 1 size: ", dims1
    print "Image 2 size: ", dims2






a = loadPDS(sys.argv[1])
b = loadPDS(sys.argv[2])

overlay(a,b)
