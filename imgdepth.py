#!/usr/bin/env python
# Wrapper script for temp2depth.

import temp2depth
import sys
import os
import os.path

currentDir = os.path.basename(os.getcwd())
dirNumber = currentDir.split('-')[0]

def usage():
    print "Usage: imgdepth.py image1 image2 ti tau albedo elevation lat lon"
    sys.exit(0)

def main():
    print ' '.join(sys.argv)
    print os.getcwd()
    if len(sys.argv) < 9:
        usage()
    img1 = sys.argv[1]
    img2 = sys.argv[2]
    ti = float(sys.argv[3])
    tau = float(sys.argv[4])
    albedo = float(sys.argv[5])
    elevation = float(sys.argv[6])
    lat = float(sys.argv[7])
    lon = float(sys.argv[8])

    runp = temp2depth.RunParams(img1, img2, ti, tau, albedo, elevation, lat, lon)
    run = temp2depth.Run(runp)
    run.doSetup()
    if run.doTemp2Depth():
        #run.doTIImages()
        run.writeResults(prefix=dirNumber)
    else:
        print "!!! Image {0} or {1} is not within model bounds, stopping".format(img1, img2)

if __name__ == '__main__':
    main()
