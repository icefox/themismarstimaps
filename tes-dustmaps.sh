#!/bin/sh
# A script to attempt to check out all the TES dust maps at once.
# Copypasta for the win!

#time dmap -o tes-dust-Ls000.vicar -fields "tot_ice tot_dust" -select "heliocentric_lon 0 30 incidence 0 85 emission 0 5 quality:atmospheric_opacity_rating 0 0 atm.nadir_opacity[1] -0.03 0.25 atm.nadir_opacity[2] -0.03 0.2 scan_len 1 1 observation_type D D det_mask 7 7 spectral_mask 0 0 quality:bolometer_lamp_anomaly 0 0" -s 2 -r 90 360 -90 0 -p avg -f f -count -vg /mars/readonly/tes/mgst/data/mgst_tabs/dataset.lst &

#time dmap -o tes-dust-Ls030.vicar -fields "tot_ice tot_dust" -select "heliocentric_lon 30 60 incidence 0 85 emission 0 5 quality:atmospheric_opacity_rating 0 0 atm.nadir_opacity[1] -0.03 0.25 atm.nadir_opacity[2] -0.03 0.2 scan_len 1 1 observation_type D D det_mask 7 7 spectral_mask 0 0 quality:bolometer_lamp_anomaly 0 0" -s 2 -r 90 360 -90 0 -p avg -f f -count -vg /mars/readonly/tes/mgst/data/mgst_tabs/dataset.lst &

#time dmap -o tes-dust-Ls060.vicar -fields "tot_ice tot_dust" -select "heliocentric_lon 60 90 incidence 0 85 emission 0 5 quality:atmospheric_opacity_rating 0 0 atm.nadir_opacity[1] -0.03 0.25 atm.nadir_opacity[2] -0.03 0.2 scan_len 1 1 observation_type D D det_mask 7 7 spectral_mask 0 0 quality:bolometer_lamp_anomaly 0 0" -s 2 -r 90 360 -90 0 -p avg -f f -count -vg /mars/readonly/tes/mgst/data/mgst_tabs/dataset.lst &

#time dmap -o tes-dust-Ls090.vicar -fields "tot_ice tot_dust" -select "heliocentric_lon 90 120 incidence 0 85 emission 0 5 quality:atmospheric_opacity_rating 0 0 atm.nadir_opacity[1] -0.03 0.25 atm.nadir_opacity[2] -0.03 0.2 scan_len 1 1 observation_type D D det_mask 7 7 spectral_mask 0 0 quality:bolometer_lamp_anomaly 0 0" -s 2 -r 90 360 -90 0 -p avg -f f -count -vg /mars/readonly/tes/mgst/data/mgst_tabs/dataset.lst &

#time dmap -o tes-dust-Ls120.vicar -fields "tot_ice tot_dust" -select "heliocentric_lon 120 150 incidence 0 85 emission 0 5 quality:atmospheric_opacity_rating 0 0 atm.nadir_opacity[1] -0.03 0.25 atm.nadir_opacity[2] -0.03 0.2 scan_len 1 1 observation_type D D det_mask 7 7 spectral_mask 0 0 quality:bolometer_lamp_anomaly 0 0" -s 2 -r 90 360 -90 0 -p avg -f f -count -vg /mars/readonly/tes/mgst/data/mgst_tabs/dataset.lst &

#time dmap -o tes-dust-Ls150.vicar -fields "tot_ice tot_dust" -select "heliocentric_lon 150 180 incidence 0 85 emission 0 5 quality:atmospheric_opacity_rating 0 0 atm.nadir_opacity[1] -0.03 0.25 atm.nadir_opacity[2] -0.03 0.2 scan_len 1 1 observation_type D D det_mask 7 7 spectral_mask 0 0 quality:bolometer_lamp_anomaly 0 0" -s 2 -r 90 360 -90 0 -p avg -f f -count -vg /mars/readonly/tes/mgst/data/mgst_tabs/dataset.lst &

#time dmap -o tes-dust-Ls180.vicar -fields "tot_ice tot_dust" -select "heliocentric_lon 180 210 incidence 0 85 emission 0 5 quality:atmospheric_opacity_rating 0 0 atm.nadir_opacity[1] -0.03 0.25 atm.nadir_opacity[2] -0.03 0.2 scan_len 1 1 observation_type D D det_mask 7 7 spectral_mask 0 0 quality:bolometer_lamp_anomaly 0 0" -s 2 -r 90 360 -90 0 -p avg -f f -count -vg /mars/readonly/tes/mgst/data/mgst_tabs/dataset.lst &

#time dmap -o tes-dust-Ls210.vicar -fields "tot_ice tot_dust" -select "heliocentric_lon 210 240 incidence 0 85 emission 0 5 quality:atmospheric_opacity_rating 0 0 atm.nadir_opacity[1] -0.03 0.25 atm.nadir_opacity[2] -0.03 0.2 scan_len 1 1 observation_type D D det_mask 7 7 spectral_mask 0 0 quality:bolometer_lamp_anomaly 0 0" -s 2 -r 90 360 -90 0 -p avg -f f -count -vg /mars/readonly/tes/mgst/data/mgst_tabs/dataset.lst &

#time dmap -o tes-dust-Ls240.vicar -fields "tot_ice tot_dust" -select "heliocentric_lon 240 270 incidence 0 85 emission 0 5 quality:atmospheric_opacity_rating 0 0 atm.nadir_opacity[1] -0.03 0.25 atm.nadir_opacity[2] -0.03 0.2 scan_len 1 1 observation_type D D det_mask 7 7 spectral_mask 0 0 quality:bolometer_lamp_anomaly 0 0" -s 2 -r 90 360 -90 0 -p avg -f f -count -vg /mars/readonly/tes/mgst/data/mgst_tabs/dataset.lst &

#time dmap -o tes-dust-Ls270.vicar -fields "tot_ice tot_dust" -select "heliocentric_lon 270 300 incidence 0 85 emission 0 5 quality:atmospheric_opacity_rating 0 0 atm.nadir_opacity[1] -0.03 0.25 atm.nadir_opacity[2] -0.03 0.2 scan_len 1 1 observation_type D D det_mask 7 7 spectral_mask 0 0 quality:bolometer_lamp_anomaly 0 0" -s 2 -r 90 360 -90 0 -p avg -f f -count -vg /mars/readonly/tes/mgst/data/mgst_tabs/dataset.lst &

#time dmap -o tes-dust-Ls300.vicar -fields "tot_ice tot_dust" -select "heliocentric_lon 300 330 incidence 0 85 emission 0 5 quality:atmospheric_opacity_rating 0 0 atm.nadir_opacity[1] -0.03 0.25 atm.nadir_opacity[2] -0.03 0.2 scan_len 1 1 observation_type D D det_mask 7 7 spectral_mask 0 0 quality:bolometer_lamp_anomaly 0 0" -s 2 -r 90 360 -90 0 -p avg -f f -count -vg /mars/readonly/tes/mgst/data/mgst_tabs/dataset.lst &

# Trying out a semi-experimental different set of parameters.
# It appears that the quality:atmospheric_opacity_rating flag will make sure this
# is good in general; see http://tes.asu.edu/mgst/index/quality.txt for more info
time dmap -o tes-dust-Ls330.vicar -fields "tot_ice tot_dust" -select "heliocentric_lon 330 360 incidence 0 85 emission 0 5 quality:atmospheric_opacity_rating 0 0 atm.nadir_opacity[1] -0.03 0.5 atm.nadir_opacity[2] -0.03 0.5 scan_len 1 1 observation_type D D det_mask 7 7 spectral_mask 0 0 quality:bolometer_lamp_anomaly 0 0 quality:temperature_profile_rating 0 0" -s 2 -r 90 360 -90 0 -p avg -f f -count -vg /mars/readonly/tes/mgst/data/mgst_tabs/dataset.lst &

