#!/usr/bin/env python
# Some functions to grab a CTX image, by product id.
# Necessary, and more complicated than necessary, because
# the actual files are divided up into DVD-sized directories
# in the PDS archive.
# We can use the http://themis-data.mars.asu.edu/ CTX index to find
# what images are in what directories.
# Other than that, more or less complements themisStamp.py
# The process in general is documented here:
# http://isis.astrogeology.usgs.gov/IsisWorkshop/index.php/Working_with_Mars_Reconnaissance_Orbiter_CTX_Data
