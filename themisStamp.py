#!/usr/bin/env python
# A script to take a latitude and longitude (point), 
# query the THEMIS stamp server appropriately, and
# return the image numbers of all decent-quality (>3)
# THEMIS nighttime images of that point, along with
# the Ls they were taken in, lat/lon.
# Also checks to make sure it has band 9.
# Anything else we need?
#
# This can be either invoked on the command line,
# where it will print out CSV of the results,
# or it can be imported as a Python library and
# you can just call getImagesAt() which will return
# a list of imagenum,ls pairs

import sys
import urllib
import csv

usage = """
   themisStamp.py lat lon ls lsRange

To get all images, just put 0 for ls and 360 for lsRange.
"""

def constructRequest(lat, lon, ls, lsRange):
    radius = 0.1
    minlat = lat - radius
    maxlat = lat + radius
    minlon = lon - radius
    maxlon = lon + radius
    p1 = "%s,%s" % (minlon, minlat)
    p2 = "%s,%s" % (minlon, maxlat)
    p3 = "%s,%s" % (maxlon, maxlat)
    p4 = "%s,%s" % (maxlon, minlat)
    points = ",".join([p1, p2, p3, p4])

    minLs = max((ls - lsRange), 0)
    maxLs = min((ls + lsRange), 360)

    if minLs > maxLs:
        temp = minLs
        minLs = maxLs
        maxLs = temp

    # This is really quite crude, but oh well.
    # Also note that it uses the internal DNS name www15
    # and so can only really be used inside the Moeur bldg network
    # It is also using the dev stamp server, which at the moment has
    # little practical impact besides making it easier for Scott to
    # troubleshoot if something goes wrong.
    # Query terms can easily be explored with http://themis-data.asu.edu/#start
    requestLine = "http://www15:8080/StampServer-dev/StampFetcher?instrument=THEMIS&imageType=IR&viewArea={0}&minsolarLongitude={1}&maxsolarLongitude={2}&minimageRating=3&minlocalTime=0&maxlocalTime=8&bands[]=9&format=CSV&user=sheath&quoteRecords=1&limit=100".format(points, minLs, maxLs)

    #print requestLine
    return requestLine


def getCSVData(lat, lon, ls, lsRange):
    url = constructRequest(lat, lon, ls, lsRange)
    #print("Requesting data for %s, %s, %s" % (lat, lon, ls))
    a = urllib.urlopen(url)
    # Potentially large amounts of data here...
    # Though 'large' is somewhat subjective.
    data = a.read()
    a.close()
    #print("Got it.")
    return data

def getUsefulData(csvdata):
    lst = csvdata.splitlines()
    dct = csv.DictReader(lst)
    records = []
    for x in dct:
        idNum = x['file_id']
        ls = x['solar_longitude']
        records.append( (idNum, ls) )
    return records

def getImagesAt(lat, lon, ls, lsRange):
    dat = getCSVData(lat, lon, ls, lsRange)
    return getUsefulData(dat)

def main():
    if len(sys.argv) < 4:
        print(usage)
        sys.exit(1)
    lat = float(sys.argv[1])
    lon = float(sys.argv[2])
    ls = float(sys.argv[3])
    lsRange = float(sys.argv[4])
    #data = getCSVData(lat, lon, ls, lsRange)
    #records = getUsefulData(data)
    imgs = getImagesAt(lat, lon, ls, lsRange)
    #print("image_id,solar_longitude")
    for x in imgs:
        (imgid,ls) = x
        print("{0}".format(imgid))

if __name__ == '__main__':
    main()
