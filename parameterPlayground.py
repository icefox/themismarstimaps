#!/usr/bin/env python

import sys
import os
import os.path

#import matplotlib.pyplot as plt
import numpy as np

import krc
from imageProcessing import *


def writeGraph(runs, stats):
    print "Graphing..."
    # So 'runs' is an array of KRC outputs
    #fig = plt.figure()
    #plt.xlabel("Ls")
    #plt.ylabel("Surface temperature (K)")
    #plt.title("Temperature of given THEMIS images vs. KRC prediction")
    #print runs
    lines = []
    # oOOOOooookay, we are going to find the slope of the temperature change
    # in each run, and graph that.

    temps = np.array([r['tk'][:,4] for r in runs])
    tt = temps.transpose()
    summerMin = 0
    summerMax = 0
    summerAvg = 0
    winterMin = 999
    winterMax = 999
    winterAvg = 999
    for i in tt:
        mn = np.min(i)
        mx = np.max(i)
        av = np.average(i)
        #print("|| {0:0.2f} || {1:0.2f} || {2:0.2f} ||".format(mn, mx, av))
        if av > summerAvg:
            summerMin = mn
            summerMax = mx
            summerAvg = av
        if av < winterAvg:
            winterMin = mn
            winterMax = mx
            winterAvg = av

    print "Min, Max, Avg"
    print("|| {0:0.2f} || {1:0.2f} || {2:0.2f} ||".format(winterMin, winterMax, winterAvg))
    print("|| {0:0.2f} || {1:0.2f} || {2:0.2f} ||".format(summerMin, summerMax, summerAvg))

    #for s in stats:
    #    (imageid, ls, mx, mn, av) = s
    #    plt.vlines(ls, mn, mx, linewidth=1.0, color='black')
    #    plt.plot(ls, mx, 'ro')
    #    plt.plot(ls, mn, 'bo')
    #    plt.plot(ls, av, 'go')
    #plt.legend(lines, map(lambda l: "{0}".format(l.get_label()), lines))
    #plt.xlim((0, 360))
    #plt.ylim((140, 220))
    #plt.show()
    #i = figToImage(fig, (800, 600))
    #i.save('t1-3.png')



def readImage(filename):
    i = loadPDS(filename)
    ir = gdalToImageAtRect(i)
    im = np.ma.masked_values(ir.image, -32768.0)
    ic = themisRad2TB(im, 9)
    return ic

def imageStats(imagefile):
    image = readImage(imagefile)
    imageid = getJankyProductID(imagefile)
    thmData = themisIdInfo(imageid)
    ls = thmData['ls']
    mx = np.ma.max(image)
    mn = np.ma.min(image)
    av = np.ma.average(image)
    print "{0}: Ls {1:3.2f} max {2:03.2f} K min {3:03.2f} K avg {4:03.2f} K".format(imageid, ls, mx, mn, av)
    return (imageid, ls, mx, mn, av)

def usage():
    print "Usage: parameterPlayground.py ti tau albedo elevation lat slope azimuth image1 ..."
    sys.exit(0)

def main():
    print ' '.join(sys.argv)
    print os.getcwd()
    if len(sys.argv) < 8:
        usage()
    ti = float(sys.argv[1])
    tau = float(sys.argv[2])
    albedo = float(sys.argv[3])
    elevation = float(sys.argv[4]) / 1000.0
    lat = float(sys.argv[5])
    slope = float(sys.argv[6])
    azi = float(sys.argv[7])
    images = sys.argv[8:]

    print "Running KRC..."
    #layers = [1,5,9,13,17,21,25]
    layers = range(1, 29) 
    krcdats = [krc.runKRC(lat, 0, elev=elevation, alb=albedo, tau=tau, ti=ti,
                        slope=slope, azimuth=azi, layer=l)
               for l in layers]
    print "Reading THEMIS images..."
    stats = [imageStats(i) for i in images]
    print "Making graph..."
    writeGraph(krcdats, stats)

if __name__ == '__main__':
    main()
