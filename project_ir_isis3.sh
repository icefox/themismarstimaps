#!/bin/bash -f

# From /themis/lib/mosaic/project_ir_isis3.sh
# plus some small hacks and all the options appropriate
# to me.
# Primarily, takes an environment variable for clon.

############################################################################
############################################################################
##
## Projection Options
##
############################################################################
############################################################################

#Cluster Parameters
projlocation=1                          #Projection Processing Location: 0=local,1=MSFF Cluster
group="mos"                             #Identification for cluster jobs (Max 5 Characters)
queuename="low"                         #Cluster queue priority
mail="-M cluster-noise@mars.asu.edu"    #Direct the output mail (Use "" for none)
clustervar=""                           #cluster -l option (Use "-l nodes=1 -l qos=virtualize" for hpc)

#Projection Defaults
projection="SimpleCylindrical"          #The projection "SimpleCylindircal, Sinusoidal, PolarStereographic"
clat=0.0                                #Center latitude *only used for PolarSterographic
#clon=0.0                                #Center longitude
if [ $clon ]
    then clon=0.0
fi
slat=0.0                                #Latitude of true scale
lattype="PLANETOCENTRIC"                #Latitude type (PLANETOCENTRIC or PLANETOGRAPHIC)
lonsys="360"                            #Longitude domain (360=0:360 180=-180:180)
imgres=100                              #Image resolution (mpp)
interp="bilinear"                       #Interpolation type: "bilinear", "bicubic", "nearest"

#Cropping by Latitudes
minlat="--"                             #Minimum Latitude set to "--" for no cropping
maxlat="--"                             #Maximum Latitude

#Clean up and Band Selection Parameters
force=0                                 #Force the re-creation of products (force=1)
bandnum="9"                             #THEMIS band number(s) [list in CSV format]
night_clean="1"                         #Flag to do advanced clean up of nighttime images
ddaw="3"                                #Processing Flags to do deplaid, destreak, autoradcorr and white noise removal
                                            #0 for deplaid/destreak
                                            #1 for deplaid/destreak/arc/wnr1
                                            #2 for deplaid/destreak/arc/wnr2
                                            #3 for deplaid w/o destreak
                                            #4 for deplaid/destreak/arc



###############################################################################
###############################################################################
##
## Change Log
##
###############################################################################
###############################################################################

#  05/03/12 - J. Hill: Spiceinit ck option changed from "cknadir=yes" to "ckrecon=yes"

#  11/02/12 - J. Hill: Projection steps re-written to use the new THEMIS IR ISIS3
#                      projection script (th_irI3geo.sh), which will now be used 
#                      for all standard THEMIS ISIS3 processing.

#  11/09/12 - J. Hill: Corrected a bug in the post-processing IF statement. It now
#                      behaves as expected for all possible combinations of night_clean
#                      and ddaw.

#  12/19/12 - J. Hill: Corrected th_irI3geo.sh inputs to be consistent with the
#                      new usage, fixed the cropping capability, and cleaned up
#                      some minor formatting issues for consistency.

#  01/10/13 - J. Hill: Rewrote the band selection section to use ISIS3 directly
#                      rather than using the bandselect_isis3.dv wrapper script.
#                      


###############################################################################
###############################################################################
##
## Map Projection Setup
##
###############################################################################
###############################################################################

source /themis/lib/mosaic/setupisis3beta.bashrc 


##
## Create a Map Template File
##

cp /themis/lib/mosaic/maptemplates/${projection}.map ${PWD}

if [ ${projection} = "SimpleCylindrical" ]
then
    mapopt=`echo "projection=${projection} clon=${clon} targopt=user targetname=Mars lattype=${lattype} resopt=mpp resolution=${imgres} londom=${lonsys}"`
elif [ ${projection} = "Sinusoidal" ]
then
    mapopt=`echo "projection=$projection clon=${clon} targopt=user targetname=Mars lattype=${lattype} resopt=mpp resolution=${imgres} londom=${lonsys}"`
elif [ ${projection} = "PolarStereographic" ]
then
    mapopt=`echo "projection=$projection clat=$clat clon=$clon targopt=user targetname=Mars lattype=$lattype resopt=mpp resolution=$imgres londom=$lonsys"`
else
    echo "Unsupported Projection Type. Exiting..."
    exit;
fi 


##
## Use the ISIS3 Map Template to Create the Map File
##

maptemplate map=${projection}.map ${mapopt}


##
## Latitude of True Scale is Not Supported by the Map Template, So We Add It Manually
##

if [ ${projection} = "SimpleCylindrical" ]
then
    sed "/CenterLongitude/ i\  TrueScaleLatitude  = $slat" ${projection}.map > ${projection}.map.tmp
    mv ${projection}.map.tmp ${projection}.map
fi


##
## Check the Map File Exists Before Continuing 
##

if [ ! -e ${PWD}/${projection}.map ] 
then
    echo "Unable to create the map projection parameter file"
    exit;
fi



###############################################################################
###############################################################################
##
## Miscellaneous Checks and Setup Before Projecting
##
###############################################################################
###############################################################################


##
## Create Abbreviations for Standard THEMIS Projection Script
##

if [ ${projection} = "SimpleCylindrical" ]
then
    proj_abbrev="SMP"
elif [ ${projection} = "Sinusoidal" ]
then
    proj_abbrev="SNU"
elif [ ${projection} = "PolarStereographic" ]
then
    proj_abbrev="POL"
else
    echo "Unsupported Projection Type. Exiting..."
    exit;
fi 


##
## Latitude/Longitude Range Buffer Setup 
##

if [ ${maxlat} != '--' ] 
then
    latrange=`echo "${minlat}:${maxlat}"`
    minlatbuff=`echo ${minlat}-1 | bc -l`
    maxlatbuff=`echo ${maxlat}+1 | bc -l`
fi


##
## Processing Flag Variable Setup
##

if [ ${night_clean} = 0 ]
then
    if [ ${ddaw} = 0 ] 
    then
	ddaw_input="-s UDDW=1 -s DEPLAID=1 -s DESTREAK=1 -s NIGHT=0 -s AutoRADCOR=0 -s WhtNoise=0 -s RECONSTITUTE=1"
    elif [ ${ddaw} = 1 ]
    then
	ddaw_input="-s UDDW=1 -s DEPLAID=1 -s DESTREAK=1 -s NIGHT=0 -s AutoRADCor=1 -s WhtNoise=1 -s RECONSTITUTE=1"
    elif [ ${ddaw} = 2 ]
    then
	ddaw_input="-s UDDW=1 -s DEPLAID=1 -s DESTREAK=1 -s NIGHT=0 -s AutoRADCor=1 -s WhtNoise=2 -s RECONSTITUTE=1"
    elif [ ${ddaw} = 3 ]
    then
	ddaw_input="-s UDDW=1 -s DEPLAID=1 -s DESTREAK=0 -s NIGHT=0 -s AutoRADCor=0 -s WhtNoise=0 -s RECONSTITUTE=1"
    elif [ ${ddaw} = 4 ]
    then
	ddaw_input="-s UDDW=1 -s DEPLAID=1 -s DESTREAK=1 -s NIGHT=0 -s AutoRADCor=1 -s WhtNoise=0 -s RECONSTITUTE=1"
    else
	echo "Invalid Processing Flag (ddaw) Value. (Acceptable values are 0-4) Exiting..."
	exit;
    fi
elif [ ${night_clean} = 1 ]
then
    if [ ${ddaw} = 0 ] 
    then
	echo "You Should NOT Perform Destreak if Night_Clean is Also Performed. Please Use ddaw=3 at the Beginning of the Projection Script. Exitting..."
	exit;
    elif [ ${ddaw} = 1 ]
    then
	echo "You Should NOT Perform Destreak if Night_Clean is Also Performed. Please Use ddaw=3 at the Beginning of the Projection Script. Exitting..."
	exit;
    elif [ ${ddaw} = 2 ]
    then
	echo "You Should NOT Perform Destreak if Night_Clean is Also Performed. Please Use ddaw=3 at the Beginning of the Projection Script. Exitting..."
	exit;
    elif [ ${ddaw} = 3 ]
    then
	ddaw_input="-s UDDW=1 -s DEPLAID=1 -s DESTREAK=0 -s NIGHT=1 -s AutoRADCor=0 -s WhtNoise=0 -s RECONSTITUTE=1"
    elif [ ${ddaw} = 4 ]
    then
	echo "You Should NOT Perform Destreak if Night_Clean is Also Performed. Please Use ddaw=3 at the Beginning of the Projection Script. Exitting..."
	exit;
    else
	echo "Invalid Processing Flag (ddaw) Value. (Acceptable values are 0-4) Exiting..."
	exit;
    fi
else
    echo "Invalid Night_Clean Variable Value. (Acceptable values are 0 or 1) Exiting..."
    exit;
fi


##
## Remind User if They are Forcing the Recreation of All Products
##

if [ ${force} = 1 ]
then 
    echo Forcing the recreation of all products
fi


##
## Check Entries in the File List
##

for i in `cat file_list`
do

  ## Check If the File is NOT Commented Out
  if [ `basename ${i} | cut -c 1` != "I" ]
  then
      continue;
  fi

  ## Check If the File Already Exists
  if [[ ! -f ${PWD}/${i}.cub || $force != 0 ]]
  then
      uname=`whoami`
      random=`bash -c 'echo ${RANDOM}'`
      name=`basename ${PWD}`_${uname}_${random}_${i}
      name=`echo ${name} | cut -c 1-35`
      dir="/tmp/${name}/"



###############################################################################
###############################################################################
##
## Create Projection Job for Cluster Submission
##
###############################################################################
###############################################################################

cat > /tmp/$i.$$ <<EOF
#!/bin/bash
source /themis/lib/mosaic/setupisis3beta.bashrc

cleanup() {
  echo 
  echo 'Script has received signal'
  echo 'Removing tmp directory: ${dir}'
  printf 'on cluster node: '
  hostname
  rm -rf ${dir}
  rmdir ${dir}
  exit 1
}

trap cleanup 9 15

echo "Hostname: \`cat \${PBS_NODEFILE}\`"


##
## Make the Tmp Directory and CD Into It
##

mkdir -p ${dir}
cd ${dir} 


##
## Use the Standard THEMIS Processing Script to Project the Image
##

if [ "${minlat}" != "--" ] 
then 
    /themis/lib/isis/th_irI3geo.sh -i ${i} -p ${proj_abbrev} -g ${dir}/${i}.cub -c ${minlatbuff}:${maxlatbuff} ${ddaw_input} -m r=${imgres} -m l=${lonsys} -m x=${clon} -m y=${clat} -m s=${slat}
else
    /themis/lib/isis/th_irI3geo.sh -i ${i} -p ${proj_abbrev} -g ${dir}/${i}.cub ${ddaw_input} -m r=${imgres} -m l=${lonsys} -m x=${clon} -m y=${clat} -m s=${slat}
fi
echo PROJECTION COMPLETE


##
## If the Projected Image Exists, Change to the Final Name
##

#if [ ! -e "${dir}/${i}.cub" ]
#then
#  mv ${dir}/${i}+${proj_abbrev}.proj.2.cub ${dir}/${i}.cub 
#fi


##
## Pull Out the Requested Band Number, If Specified
## 

if [ "${bandnum}" != "0" ] 
then
  crop from=${dir}/${i}.cub+${bandnum} to=${dir}/${i}.bandnum.cub -verbose
  mv ${dir}/${i}.bandnum.cub ${dir}/${i}.cub
  echo BAND SELECTION COMPLETE
fi


##
## If the Projected Image Has a File Size, Move the Cube to the Correct Directory
##
      
if [ \`du ${dir}/${i}.cub | awk '{print \$1}'\` != "0" ]
then 
  mv ${dir}/${i}.cub $PWD
  echo FILE MOVE COMPLETE
fi
rm -rf ${dir}
EOF



###############################################################################
###############################################################################
##
## Submit Projection Jobs (Local or MSFF Cluster)
##
###############################################################################
###############################################################################


  if [ "${projlocation}" == "0" ]
  then
      chmod 777 /tmp/$i.$$
      cat /tmp/$i.$$ | sed 's/cat ${PBS_NODEFILE}/hostname/g' > /tmp/$i.$$
      /tmp/$i.$$
      rm /tmp/$i.$$
  fi

  if [ "${projlocation}" == "1" ]
  then
      name2=${group}_${i}
      name2=`echo ${name2} | cut -c 1-15`
      #qsub ${mail} ${clustervar} -N ${name2} -j oe -W umask=002 -q${queuename} /tmp/$i.$$
      /themis/lib/mosaic/qsub_wrap.pl -d 500 -R 60 ${mail} ${clustervar} -N ${name2} -j oe -l walltime=00:60:00 -W umask=002 -q${queuename} /tmp/$i.$$
      rm /tmp/$i.$$
  fi



###############################################################################
###############################################################################
##
## Close Remaining Loops and End Script
##
###############################################################################
###############################################################################

  else
    echo skipping ${i} because it is complete
  fi

done



###############################################################################
################################ End of Script ################################
###############################################################################
