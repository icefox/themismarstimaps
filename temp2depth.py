import math
import os


from osgeo import gdal
from osgeo import ogr
from osgeo import osr
from osgeo import gdal_array
from osgeo import gdalconst as gdc


# Magic to set matplotlib to not complain about an X display
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import Image
import ImageDraw
import ImageFont
import numpy as np
import scipy.interpolate as ip
import scipy.weave as weave


from imageProcessing import *
import krc


def overlay(imgrect1, imgrect2):
    """Takes two imageProcessing.ImageAtRect objects and returns a pair 
of numpy arrays: The contents each ImageAtRect each with the other's mask 
added and any blank edges trimmed off.
Essentially, places two images atop each other in some coordinate system,
then cookie-cutters out the segment of each image that overlaps the other.
"""
    # This creates two new images from the intersecting rectangle of the given images.
    intersection = imgrect1.rect.intersect(imgrect2.rect)
    i1 = ImageAtRect.zero(intersection)
    i2 = ImageAtRect.zero(intersection)

    i1.image_insert(imgrect1)
    i2.image_insert(imgrect2)

    # This creates and merges the masks
    (maskedArr1, maskedArr2) = combineMask(i1.image, i2.image)

    # Then we trim off the masked edges and we're done!
    trimmed1 = trimEdges(maskedArr1)
    trimmed2 = trimEdges(maskedArr2)

    return (trimmed1, trimmed2)


def imageIsNotFrozen(tid):
    """Takes a THEMIS image id and queries the database, returning true
if the surf_temp_avg value is above 160 K.
"""
    t = themisIdInfo(tid)
    temp = t['avg_btemp']
    print "Image id {0}, average temp {1}".format(tid, temp)
    return temp > 160


class RunParams(object):
    """An object that contains all physical input values for a Run."""
    def __init__(self, imgfile1, imgfile2, ti, tau, albedo, elevation, lat, lon):
        print imgfile1, imgfile2
        self.imgfile1 = imgfile1
        self.imgfile2 = imgfile2
        self.imgid1 = os.path.splitext(imgfile1)[0][:9]
        self.imgid2 = os.path.splitext(imgfile2)[0][:9]
        self.ti = ti
        self.tau = tau
        self.albedo = albedo
        self.elevation = elevation
        self.lat = lat
        self.lon = lon

        info1 = themisIdInfo(self.imgid1)
        info2 = themisIdInfo(self.imgid2)
        self.ls1 = info1['ls']
        self.solarTime1 = info1['solar_time']
        self.avgTemp1 = info1['avg_btemp']
        self.ls2 = info2['ls']
        self.solarTime2 = info2['solar_time']
        self.avgTemp2 = info2['avg_btemp']

        self.minTI = 20
        self.maxTI = 1000
        self.tiSamples = 30
        #self.tiSamples = 10


class Run(object):
    """An object that contains all input, output and intermediate products 
for a run, code to actually execute the run, and some useful helper 
functions."""
    def __init__(self, runParams):
        self.params = runParams
        self.outputWidth = 1200
        self.outputHeight = 800
        self.captionHeight = 100
        
    def doSetup(self):
        """Does all the data processing leading up to a run."""
        print "Doing setup"
        img1 = self.params.imgfile1
        img2 = self.params.imgfile2

        gdal1 = loadPDS(img1)
        gdal2 = loadPDS(img2)
        rect1 = gdalToImageAtRect(gdal1)
        rect2 = gdalToImageAtRect(gdal2)
        (trimmed1, trimmed2) = overlay(rect1, rect2)
        
        # XXX: Check for non-overlapping images
        cal1 = themisRad2TB(trimmed1, 9)
        cal2 = themisRad2TB(trimmed2, 9)

        # Image 1 should be the 'warm' image, image 2 the 'cold' one.
        # So the temperature difference will be negative, because the
        # temperature has decreased in the time between them.
        self.img1 = cal1
        self.img2 = cal2
        self.diff = cal2 - cal1


        self.layercount = 28
        #self.layercount = 14
        
        

    def resampleToDepth(self, results, tis, layercount, season1, season2, timeOfDay1, timeOfDay2):
        """This function takes a pile of KRC run data
and returns a 2d array of temperatures indexed by depth in meters
and TI, with -1 as an invalid value marker.
It also interpolates them to be at the exact time of day given.
"""
        self.depths = []
        self.season1Temps = []
        self.season2Temps = []
        minTimeOfDay1 = math.floor(timeOfDay1)
        maxTimeOfDay1 = math.ceil(timeOfDay1)
        minTimeOfDay2 = math.floor(timeOfDay2)
        maxTimeOfDay2 = math.ceil(timeOfDay2)

        for i in range(len(tis)):
            season1Temps = []
            season2Temps = []
            depths = []
            for j in range(layercount):
                # 0 is skin depths, 1 is meters...
                depths.append(results[i][j]['layerTop'][j, 1])
                temp1min = results[i][j]['tk'][season1, minTimeOfDay1]
                temp1max = results[i][j]['tk'][season1, maxTimeOfDay1]
                f1 = ip.interp1d([minTimeOfDay1, maxTimeOfDay1],
                                [temp1min, temp1max])
                temp1 = f1(timeOfDay1)

                temp2min = results[i][j]['tk'][season2, minTimeOfDay2]
                temp2max = results[i][j]['tk'][season2, maxTimeOfDay2]
                f2 = ip.interp1d([minTimeOfDay2, maxTimeOfDay2],
                                [temp2min, temp2max])
                temp2 = f2(timeOfDay2)

                season1Temps.append(temp1)
                season2Temps.append(temp2)
            self.depths.append(depths)
            self.season1Temps.append(season1Temps)
            self.season2Temps.append(season2Temps)

        self.depths = np.array(self.depths)
        # This should now be an array of temperatures, by TI and layer
        self.season1Temps = np.array(self.season1Temps)
        self.season2Temps = np.array(self.season2Temps)

        # Okay, we have a 2d array of tk's, one axis season and another
        # axis time of day.  We can ditch the time of day axis because
        # it's always the same (sort of).  We want one axis of TI and one of layer
        # depths.  Sooooo...
        # Now for each TI we make an interpolation from depth to temp.
        # Then we can resample our temperature grid to units of depth
        # rather than layer.
        #mindepth = np.min(self.depths)
        #maxdepth = np.max(self.depths)
        
        # Okay...  I THINK that we have to limit this to the range valid for the
        # lowest TI value.
        # Thing is, say the surface TI is 150.  We get a fit for surface TI=150 and
        # depth = whatever.  But then it tries greater and greater depths, which means
        # greater and greater TI's; at some point a TI of 150 is no longer a possible
        # output, and so it returns a TI of 300, or 500, or whatever.  
        mindepth = 0.001 #np.min(self.depths[0])
        maxdepth = np.max(self.depths[0]) - mindepth
        steps = 100
        #steps = 20
        #self.newDepths = np.linspace(mindepth, maxdepth, num=steps)

        # Make a nonlinear depth curve...
        # This goes from 1 to 2, minus 1 to put it in [0, 1]
        expDepths = np.logspace(0, math.log(2), num=steps, base=math.e) - 1
        # then gets normalized to be in [mindepth, maxdepth]
        expDepths = (expDepths * maxdepth) + mindepth
        self.newDepths = expDepths

        
        #nlDepths = np.arange(0, 1, 1.0/steps)
        #nlDepths = nlDepths ** 2
        #nlDepths = (nlDepths * maxdepth) + mindepth
        #print nlDepths
        #self.newDepths = nlDepths

        s1DepthTempInterp = [ip.interp1d(depth, temp,
                                         copy=False, bounds_error=False, fill_value=-1)
                             for (depth, temp) in zip(self.depths, self.season1Temps)]

        s2DepthTempInterp = [ip.interp1d(depth, temp,
                                         copy=False, bounds_error=False, fill_value=-1)
                             for (depth, temp) in zip(self.depths, self.season2Temps)]

        self.season1ResampledTemps = \
            np.array(map(lambda f: f(self.newDepths), s1DepthTempInterp))
        self.season2ResampledTemps = \
            np.array(map(lambda f: f(self.newDepths), s2DepthTempInterp))

        return (self.newDepths, self.season1ResampledTemps, self.season2ResampledTemps)
               

    def doKRCRuns(self):
        """This actually runs the KRC simulations described in the params
structure and returns the results collection as well as the TI's it ran at.
"""
        # KRC takes elevation in km, we always use meters.
        elevation = self.params.elevation / 1000.0

        #tis = np.arange(self.params.minTI, self.params.maxTI, self.params.tiRes)
        tis = np.linspace(self.params.minTI, self.params.maxTI, num=self.params.tiSamples)
        results = []
        # Now instead of running 28 KRC's per run,
        # we're doing 28 * 30 or so...  oog.
        # This makes the idea of threading out separate KRC runs more appealing,
        # but collating the data from that is trickier; I want something basically
        # like map/reduce.
        for ti in tis:
            accm = []
            for lay in range(self.layercount):
                #print("Running sim for layer={0} and TI={1}".format(lay, ti))
                res = krc.runKRC(
                    self.params.lat, self.params.lon, elev=elevation,
                    alb=self.params.albedo, tau=self.params.tau, 
                    slope=0.0, azimuth=0.0, 
                    ti=ti, layer=lay)
                accm.append(res)
            results.append(accm)
        return (results, tis)

    def doTemp2DepthNew(self):
        # First, actually do the KRC runs and get the results out of them.
        print "Doing KRC runs"
        (self.results, self.tis) = self.doKRCRuns()

        # Figure out what KRC 'seasons' our Ls numbers correspond to
        season1Offset = (self.params.ls1 / 360.0) * 80
        season2Offset = (self.params.ls2 / 360.0) * 80
        print "Season 1: {0:.1f}, season 2: {1:.1f}".format(season1Offset, season2Offset)
        # The first value of the time array is at 1 am.
        # So arr[0] is 1 am.
        # XXX: Or at least, Davinci says it is...  I'm not so sure.
        # And this scares me because it makes a HUGE difference.
        # However, I'm not going to be able to find out better, so.
        timeOfDay1 = self.params.solarTime1
        timeOfDay2 = self.params.solarTime2
        print "Solar times:", timeOfDay1, timeOfDay2

        # This function yanks out the temperatures and resamples them to
        # be in layers of uniform depth, instead of... whatever the hell
        # KRC did.  Layers of uniform skin depth, except not.
        print "Resampling from layer to depth"
        (self.newdepths, self.s1temps, self.s2temps) = self.resampleToDepth(self.results, self.tis, self.layercount, season1Offset, season2Offset, timeOfDay1, timeOfDay2)
        self.temps = [self.s1temps, self.s2temps]

        # Now we check whether the image temperatures are even within
        # bounds of the predictions, and if the average temps are not,
        # then we just return since the results will be bad anyway.
        #season1min = np.min(self.season1Temps)
        #season1max = np.max(self.season1Temps)
        #season2min = np.min(self.season2Temps)
        #season2max = np.max(self.season2Temps)
        #if self.params.avgTemp1 < season1min \
        #        or self.params.avgTemp1 > season1max \
        #        or self.params.avgTemp2 < season2min \
        #        or self.params.avgTemp2 > season2max:
        #    return False
        
        print self.s1temps.shape
        print self.s2temps.shape
        print self.newdepths

        # Okay.  So instead of all this resampling and shit, we just try
        # a simple match
        # For each pixel
        #    For each model result
        #       See which model result is closest to the temp
        #       If the result is closer than existing one:
        #          Record which result it is and add the magnitude of the difference to the accm
        # 
        # But this doesn't work because each model result is not unique!
        # So.  For each pixel...
        # For each model result
        #    For each image
        #       generate the difference between the image and the model result
        #       save it somewhere
        #    If the sum of the differences is better than the previous best model,
        #       Save the current model parameters over previous best
        #       
        #accumulatedError = np.ma.zeros(self.img1.shape, dtype=np.float32)
        bestMatchDifference = np.ma.zeros(self.img1.shape, dtype=np.float32)
        bestMatchDifference[:] = 9999.0
        bestMatchTI = np.ma.zeros(self.img1.shape, dtype=np.float32)
        bestMatchDepth = np.ma.zeros(self.img1.shape, dtype=np.float32)
        images = [self.img1, self.img2]
        (diffx, diffy) = self.img1.shape
        diffz = len(images)
        for (ti, tiIdx) in zip(self.tis, range(len(self.tis))):
            for (depth, depthIdx) in zip(self.newdepths, range(len(self.newdepths))):
                #currentDiff = np.ma.zeros(self.img1.shape, dtype=np.float32)
                print "Working on layer with depth", depth, ti, [temp[tiIdx, depthIdx] for temp in self.temps]
                currentDiff = np.ma.zeros((diffx, diffy, diffz), dtype=np.float32)
                for (i,img,modelResultFor) in zip(range(diffz), images, self.temps):
                    currentDiff[:,:,i] = np.ma.abs(img - modelResultFor[tiIdx,depthIdx])
                totalDiffs = np.ma.sum(currentDiff, axis=2)
                betterGuessesThanCurrent = bestMatchDifference > totalDiffs
                bestMatchDifference[betterGuessesThanCurrent] = totalDiffs[betterGuessesThanCurrent]
                bestMatchTI[betterGuessesThanCurrent] = ti
                bestMatchDepth[betterGuessesThanCurrent] = depth


        self.bDiff = bestMatchDifference
        self.bTI = bestMatchTI
        self.bTI.mask = self.bDiff.mask
        self.bDepth = bestMatchDepth
        self.bDepth.mask = self.bDiff.mask
        #for (depth, temps) in zip(self.newdepths
        return True
        

    def doTemp2Depth(self):
        """Actually does the calculation and such.
This version uses the algorithm suggested by Josh Bandfield,
of generating matches for both ti and layer depths, and then
matching them up against observed temps for each season."""
        # First, actually do the KRC runs and get the results out of them.
        print "Doing KRC runs"
        (self.results, self.tis) = self.doKRCRuns()

        # Figure out what KRC 'seasons' our Ls numbers correspond to
        season1Offset = (self.params.ls1 / 360.0) * 80
        season2Offset = (self.params.ls2 / 360.0) * 80
        print "Season 1: {0:.1f}, season 2: {1:.1f}".format(season1Offset, season2Offset)
        # The first value of the time array is at 1 am.
        # So arr[0] is 1 am.
        # XXX: Or at least, Davinci says it is...  I'm not so sure.
        # And this scares me because it makes a HUGE difference.
        # However, I'm not going to be able to find out better, so.
        timeOfDay1 = self.params.solarTime1
        timeOfDay2 = self.params.solarTime2
        print "Solar times:", timeOfDay1, timeOfDay2

        # This function yanks out the temperatures and resamples them to
        # be in layers of uniform depth, instead of... whatever the hell
        # KRC did.  Layers of uniform skin depth, except not.
        print "Resampling from layer to depth"
        (self.newdepths, self.s1temps, self.s2temps) = self.resampleToDepth(self.results, self.tis, self.layercount, season1Offset, season2Offset, timeOfDay1, timeOfDay2)

        # Now we check whether the image temperatures are even within
        # bounds of the predictions, and if the average temps are not,
        # then we just return since the results will be bad anyway.
        season1min = np.min(self.season1Temps)
        season1max = np.max(self.season1Temps)
        season2min = np.min(self.season2Temps)
        season2max = np.max(self.season2Temps)
        if self.params.avgTemp1 < season1min \
                or self.params.avgTemp1 > season1max \
                or self.params.avgTemp2 < season2min \
                or self.params.avgTemp2 > season2max:
            return False
        
        s1tt = self.s1temps.transpose()
        s2tt = self.s2temps.transpose()
        
        # Now we make interpolations from temperature to TI for each layer depth.
        # This gets less elegant as we have to remove invalid values...
        # These are caused by the fact that depths vary with TI, so we don't
        # have the same depth range for every TI run.
        # Stupid KRC.
        #
        # We also make the inverse interpolation functions, so we can then
        # plug TI's in and get temps out to check how close the results are.
        self.s1TempTIInterps = []
        self.s1TITempInterps = []
        for temps in s1tt:
            t = temps[temps != -1]
            i = ip.interp1d(temps, self.tis,
                            copy=False, bounds_error=False, fill_value=-1)
            self.s1TempTIInterps.append(i)

            i2 = ip.interp1d(self.tis, temps, copy=False, bounds_error=False, fill_value=-1)
            self.s1TITempInterps.append(i2)

        self.s2TempTIInterps = []
        self.s2TITempInterps = []
        for temps in s2tt:
            t = temps[temps != -1]
            i = ip.interp1d(temps, self.tis,
                            copy=False, bounds_error=False, fill_value=-1)
            self.s2TempTIInterps.append(i)

            i2 = ip.interp1d(self.tis, temps, copy=False, bounds_error=False, fill_value=-1)
            self.s2TITempInterps.append(i2)

        # So we now have, for each depth, an interpolation from temp to TI.
        # Now, for each depth, we make a TI prediction for our two seasons based 
        # on the temperatures in our images.
        # The depth where the TI predictions are closest is the one we want!
        (numTIs, numDepths) = self.s1temps.shape
        xdim, ydim = self.img1.shape
        #self.res1 = np.ma.zeros((xdim, ydim, numDepths), dtype=np.float32)
	self.avgRes1 = np.ma.zeros((numDepths))
	self.stdRes1 = np.ma.zeros((numDepths))
        #self.res2 = np.ma.zeros((xdim, ydim, numDepths), dtype=np.float32)
	self.avgRes2 = np.ma.zeros((numDepths))
	self.stdRes2 = np.ma.zeros((numDepths))
        self.diffs = np.ma.zeros((xdim, ydim, numDepths), dtype=np.float32)
        depthIndices = range(numDepths)

        print "Img1 shape", self.img1.shape
        print "Img2 shape", self.img2.shape

        for (s1interp, s2interp, depth, i) in zip(self.s1TempTIInterps, self.s2TempTIInterps, self.newdepths, depthIndices):
            print "Figuring out stuffs for layer {0}, depth {1}".format(i, depth)

            s1TIPrediction = s1interp(self.img1)
            #self.res1[:,:,i] = s1TIPrediction
            self.avgRes1[i] = s1interp(np.ma.average(self.img1)) #np.average(s1TIPrediction)
            self.stdRes1[i] = s1interp(np.ma.std(self.img1)) #np.std(s1TIPrediction)
            #s1masked = np.ma.masked_values(s1TIPrediction, -1)
            s2TIPrediction = s2interp(self.img2)
            self.avgRes2[i] = s2interp(np.ma.average(self.img2)) # np.average(s2TIPrediction)
            self.stdRes2[i] = s2interp(np.ma.average(self.img2))  # np.std(s2TIPrediction)
            #self.res2[:,:,i] = s2TIPrediction
            #s2masked = np.ma.masked_values(s2TIPrediction, -1)
            # Since we have conveniently chosen the invalid values for each interpolation
            # to have a difference of like 18000, so the difference of invalid values will
            # always be huge and thus never be chosen by the argmin() below.
            # ...unless ALL the options are invalid.  What happens then?  Well, we get a difference
            # of 18000, and that will be the best fit, so that will get chosen...
            # XXX: So we need to detect when that is happening and do something about it.
            #diff = np.abs(s1TIPrediction - s2TIPrediction)
            #diff = s1TIPrediction - s2TIPrediction
            #diff = np.ma.abs(s1masked - s2masked)
            #self.diffs[:,:,i] = diff
            self.diffs[:,:,i] = np.abs(s1TIPrediction - s2TIPrediction)

        #self.res1 = np.ma.masked_values(self.res1, -1)
        #self.res2 = np.ma.masked_values(self.res2, -1)
        # This step can use a LOT of memory if the input is huge
        print "Masking diffs"
        self.diffs = np.ma.masked_values(self.diffs, 0)

        # Boy I'm dumb for not thinking of this before.
        # the PROBLEM here is that np.ma.argmin() returns 0 if ALL the values
        # in the target array are masked!  So if there were NO good fits, then this
        # will still return 0 all the time!
        # So, we just check for pixels where self.diffs is always masked, and if so,
        # mask that pixel out.
        print "Finding invalid pixels"
        print self.diffs.shape
        print self.diffs.mask.shape
        if self.diffs.mask.shape != ():
            self.invalidPixels = np.all(self.diffs.mask, axis=2)
        else:
            self.invalidPixels = np.zeros(self.img1.shape, dtype=np.bool)
        print "Making mask"
        imageMask = np.logical_or(self.img1.mask, self.invalidPixels)
        # This step can use a LOT of memory if the input is huge
        print "Generating output"

        self.layerOutput = np.ma.array(np.argmin(self.diffs, axis=2))
        print "Generating output mask"
        self.layerOutput.mask = imageMask

        # Translate depth layer # into actual depth
        print "Making depth output"
        self.depthOutput = np.ma.array(self.newdepths[self.layerOutput])
        print "Making depth output mask"
        self.depthOutput.mask = imageMask
        
        self.output = self.depthOutput
        # All right, now we do that all over again to generate new temp images from
        # our modelled values...
        # We need the TI images for that.
        self.s1TI = np.ma.zeros(self.img1.shape)
        self.s2TI = np.ma.zeros(self.img1.shape)
        for (s1interp, s2interp, i) in zip(self.s1TempTIInterps, self.s2TempTIInterps, depthIndices):
            print "Generating TI images, layer", i
            where = self.layerOutput == i
            # This sort of indexing-using-a-boolean-array is surprisingly fast.
            self.s1TI[where] = s1interp(self.img1[where])
            self.s2TI[where] = s2interp(self.img2[where])

        self.s1TI = np.ma.masked_values(self.s1TI, -1.0)
        self.s2TI = np.ma.masked_values(self.s2TI, -1.0)

        # Now for error analysis, we average our two TI solutions together.
        # Then we turn that TI into a temperature at each season, then
        # our error in temperature is the difference between those projected
        # temps at our average TI, and the actual observed temps.
        # This might not be the best way but it's the only one I can think of.
        self.avgTI = np.ma.average([self.s1TI, self.s2TI], axis=0)

        self.reverseTempResult1 = np.ma.zeros(self.img1.shape)
        self.reverseTempResult2 = np.ma.zeros(self.img1.shape)

        for (s1interp, s2interp, i) in zip(self.s1TITempInterps, self.s2TITempInterps, depthIndices):
            print "Doing inverse modelling for for layer {0}".format(i)
            where = self.layerOutput == i
            self.reverseTempResult1[where] = s1interp(self.avgTI[where])
            self.reverseTempResult2[where] = s2interp(self.avgTI[where])

        self.reverseTempDiff1 = np.ma.abs(self.img1 - self.reverseTempResult1)
        self.reverseTempDiff2 = np.ma.abs(self.img2 - self.reverseTempResult2)

        self.tempError = self.reverseTempDiff1 + self.reverseTempDiff2

        # This return is important!
        # We return False way up there ^^^ if the input images aren't within
        # the model bounds.
        return True




    def getInvalidValues(self):
        """This returns a 3-band color image: Red is places where the
observed temperature difference is greater than the model,
blue is where it is less than the model, and green is where the observed
temps in EITHER image are below the CO2 ice temperature of 160 K.
Must be run after doTemp2Depth()."""
        # Create a mask to avoid temperatures lower than CO2 ice freezing temp
        self.iceMask = np.ma.logical_or(self.img1 < 160,  self.img2 < 160)


        # Create a mask to avoid deltaT's out of bounds of predicted values
        # Numpy can do some operations on normal lists!  :D
        #deltaTMin = np.min(self.deltat)
        #deltaTMax = np.max(self.deltat)
        #self.oobLowMask = self.diff < deltaTMin
        #self.oobHighMask = self.diff > deltaTMax

        invalidMask = np.ma.dstack((np.zeros(self.iceMask.shape), np.zeros(self.iceMask.shape), self.iceMask))
        vals = np.ma.zeros(invalidMask.shape, dtype=np.uint8)
        #vals[invalidMask] = 192
        #vals.mask = np.ma.dstack((self.diff.mask, self.diff.mask, self.diff.mask))

        #self.invalidValues = vals
        return invalidMask
        

    def getLsStrings(self):
        """Created formatted strings describing the Ls's of the run."""
        s1 = "Warm season Ls {0:.1f}".format(self.params.ls1)
        s2 = "Cold season Ls {0:.1f}".format(self.params.ls2)
        return (s1, s2)


    def getDepthTIPlot(self):
        """Returns a matplotlib Figure that shows the TI vs. layer depth curves
for a given temperature --the average temperature of img1 and img2.
"""
        plt.figure()
        # Plot TI predictions per depth, +/- 1 standard deviation
        label1 = "Warm season avg, {0} K".format(np.ma.average(self.img1))
        label2 = "Cool season avg, {0} K".format(np.ma.average(self.img2))
        std1 = self.stdRes1
        std2 = self.stdRes2 
        plt.plot(self.newDepths, self.avgRes1, 'r', label=label1)
        plt.fill_between(self.newdepths, self.avgRes1+std1,
                         self.avgRes1-std1, facecolor='red', alpha=0.2)

        plt.plot(self.newDepths, self.avgRes2, 'b', label=label2)
        plt.fill_between(self.newdepths, self.avgRes2+std2,
                         self.avgRes2-std2, facecolor='blue', alpha=0.2)

        plt.title("Predicted TI & depth for given temp\nLine is image average temperature,\nshaded region is +/- 1 sigma")
        plt.xlabel("Layer depth (m)")
        plt.ylabel("Thermal inertia")
        plt.grid(True)
        plt.legend(loc='best')
        return plt.gcf()

    def getYearlyTempPlot(self):
        """Returns a matplotlib Figure that shows the surface
temperature vs. Ls for the given region, with many lines of different
layer thickness on it.
Includes vertical lines for the warm and cool Ls's.
"""
        plt.figure()
        plot = plt.subplot(1, 1, 1)
        lss = np.linspace(0, 360, num=80)
        # These are the indices of the TI's we use, I think...
        tis = [1, 5, 11, 17]
        colors = ['red', 'green', 'blue', 'orange', 'black']
        legendLines = []
        legendLabels = []
        for ti, c in zip(tis, colors):
            # For this TI we display every 3rd layer, surface temperature, at all Ls's, 5 AM
            tiResult = self.results[ti]
            res = [tiResult[i]['tk'][:,4] for i in range(0, 26, 3)]
            res = np.array(res)
            print lss.shape
            print res.shape
            plt.xlim((0, 360))
            lines = plt.plot(lss, res.transpose(), color=c)
            legendLines.append(lines[0])
            legendLabels.append("TI {0:.0f}".format(self.tis[ti]))
            

        # Throw lines on the Ls's of the warm and cool seasons
        #(miny, maxy) = plot.get_ybound()
        mx1 = np.ma.max(self.img1)
        mn1 = np.ma.min(self.img1)
        av1 = np.ma.average(self.img1)
        mx2 = np.ma.max(self.img2)
        mn2 = np.ma.min(self.img2)
        av2 = np.ma.average(self.img2)
        plt.plot(self.params.ls1, mx1, 'ro')
        plt.plot(self.params.ls1, mn1, 'bo')
        plt.plot(self.params.ls1, av1, 'go')
        plt.plot(self.params.ls2, mx2, 'ro')
        plt.plot(self.params.ls2, mn2, 'bo')
        plt.plot(self.params.ls2, av2, 'go')
        plt.vlines(self.params.ls1, mn1, mx1, linewidth=2.0, color='r')
        plt.vlines(self.params.ls2, mn2, mx2, linewidth=2.0, color='b')

        plt.title("Modelled 5 AM surface temperatures")
        plt.xlabel("Ls")
        plt.ylabel("Surface temp (K)")
        plt.legend(legendLines, legendLabels, loc='best')
        return plt.gcf()

    def getImageHistogramPlot(self):
        """Returns a matplotlib Figure that shows the temperature
histograms for the given input images, and compares them to the 
predicted temperature ranges.
"""
        minval = min(np.ma.min(self.img1), np.ma.min(self.img2))
        maxval = max(np.ma.max(self.img1), np.ma.max(self.img2))
        (hist1, bins1) = np.histogram(self.img1, range=(minval, maxval), bins=50)
        (hist2, bins2) = np.histogram(self.img2, range=(minval, maxval), bins=50)
        (hist3, bins3) = np.histogram(self.reverseTempResult1, range=(minval,maxval), bins=50)
        (hist4, bins4) = np.histogram(self.reverseTempResult2, range=(minval, maxval), bins=50)
        print hist3
        print hist4
        plt.figure()
        plot = plt.subplot(2, 1, 1)
        (s1, s2) = self.getLsStrings()
        # For some inconvenient reason numpy returns the bin EDGES, so 10 values
        # give 11 edge values for bins.  So screw it, we just ditch the last one.
        plt.plot(bins1[:-1], hist1, 'r-', label=s1)
        plt.plot(bins2[:-1], hist2, 'b-', label=s2)
        plt.plot(bins3[:-1], hist3, 'r--', label="Modelled temp 1")
        plt.plot(bins4[:-1], hist4, 'b--', label="Modelled temp 2")
        plt.title("Observed vs. modelled temperatures")
        plt.xlabel("Observed surface temp (K)")
        plt.ylabel("Counts")
        plt.grid(True)
        plt.legend(loc='best')

        season1min = np.min(self.season1Temps)
        season1max = np.max(self.season1Temps)
        season2min = np.min(self.season2Temps)
        season2max = np.max(self.season2Temps)
        (miny, maxy) = plot.get_ybound()
        plt.fill_between((season1min, season1max), (maxy, maxy), facecolor='red', alpha=0.2)
        plt.fill_between((season2min, season2max), (maxy, maxy), facecolor='blue', alpha=0.2)

        # Throw a line on the (overestimated) CO2 freezing temp.
        plt.vlines(160, miny, maxy, linewidth=2.0)

        #s1timin = np.ma.min(self.s1TI)
        #s1timax = np.ma.max(self.s1TI)

        #s2timin = np.ma.min(self.s2TI)
        #s2timax = np.ma.max(self.s2TI)

        #timin = min(s1timin, s2timin)
        #timax = max(s1timax, s2timax)
        #(s1hist, s1bins) = np.histogram(self.s1TI, range=(timin, timax), bins=50)
        #(s2hist, s2bins) = np.histogram(self.s2TI, range=(timin, timax), bins=50)
        timin = np.ma.min(self.avgTI)
        timax = np.ma.max(self.avgTI)
        (tihist, tibins) = np.histogram(self.avgTI, range=(timin, timax), bins=50)
        plot = plt.subplot(2, 1, 2)
        plt.plot(tibins[:-1], tihist, 'r-')
        #plt.plot(s2bins[:-1], s2hist, 'b--')
        plt.title("Calculated thermal inertias")
        plt.xlabel("Thermal inertia")
        plt.ylabel("Counts")
        plt.grid(True)

        #mn = np.min(self.deltat)
        #mx = np.max(self.deltat)
        #(miny, maxy) = plot.get_ybound()
        #plt.fill_between((mn, mx), (maxy, maxy), facecolor='green', alpha=0.2)
        return plt.gcf()


    def decorateImage(self, img):
        """Takes a byte array image, then plops a 10 km scalebar and
a north arrow on it.
This is easy because all our images are 100 m/pixel and projected with
north being up, and the lower-left corner conveniently empty."""
        (h,w) = img.shape
        img = img.copy()
        scaleHeight = 15
        # 10,000 meters / 100 meters/pixel
        scaleWidth = 10000 / 100

        xoff = 30
        yoff = 30

        white = np.ma.max(img)

        img[h-(yoff+scaleHeight):h-yoff, xoff:xoff+scaleWidth] = white
        return img

    def getScaleBar(self, mini, maxi):
        """Okay we really don't have an actual drawing
library, so this is going to suck.  But here goes."""
        #img = img.copy()
        xoff = 30
        yoff = 30
        h = 20
        w = 120
        scl = np.ma.array(np.linspace(mini, maxi, num=w))
        #scl = scl.reshape((h))
        wideScale = np.ma.vstack([scl] * h)
        return wideScale


    def addScaleBar(self, img):
        """This is really dumb but it seems to work."""
        img = img.copy()
        (h, w) = img.shape

        scl = self.getScaleBar(np.ma.min(img), np.ma.max(img))
        (scaleHeight, scaleWidth) = scl.shape
        xoff = 30
        yoff = 30

        img[h-(yoff+scaleHeight):h-yoff, xoff:xoff+scaleWidth] = scl
        return img


    def colorizeWithScaleBar(self, img, unit=''):
        """This is REALLY REALLY dumb but it seems to work."""
        img = img.copy()
        (h, w) = img.shape

        imgWithScale = self.addScaleBar(img)
        colorImg = rgbArrToImage(colorize(imgWithScale))
        mini = np.ma.min(img)
        maxi = np.ma.max(img)
        minText = renderText("{0:.3f} {1}".format(mini, unit))
        maxText = renderText("{0:.3f} {1}".format(maxi, unit))
        colorImg.paste(minText, (20, h-30))
        colorImg.paste(maxText, (140, h-30))
        return colorImg


    def getInputResult(self):
        """This produces a nice output image with the two source images,
and the difference between them."""
        imgs = []
        img1 = self.decorateImage(sstretch(self.img1))
        img2 = sstretch(self.img2)
        diff = sstretch(self.diff)
        imgs.append(gArrToImage(img1))
        imgs.append(gArrToImage(img2))
        imgs.append(gArrToImage(diff))
        #for i in [self.img1, self.img2, self.output]:
        #    i = sstretch(i)
        #    (h, w) = i.shape
        #    img = Image.frombuffer("L", (w, h), i, "raw", "L", 0, 1)
        #    imgs.append(img)
        img = xImages(imgs)
        #img.show()

        imageGeneralInfo = "Scalebar is 10 km, {lon:3.2f} E {lat:2.2f}"
        caption = "Image {idnum}, Ls {ls:.0f}, min temp {mintemp:.1f} K, max temp {maxtemp:.1f} K, solar time {solartime}"
        imageDiff = "Temperature difference between seasons: min {mindiff:.0f} K, max {maxdiff:.0f} K"
        lines = []
        lines.append(
            imageGeneralInfo.format(lon=self.params.lon, 
                                    lat=self.params.lat))
        lines.append(
            caption.format(idnum=self.params.imgid1, 
                           ls=self.params.ls1, 
                           mintemp=np.ma.min(self.img1), 
                           maxtemp = np.ma.max(self.img1),
                           solartime=self.params.solarTime1))
        lines.append(
            caption.format(idnum=self.params.imgid2, 
                           ls=self.params.ls2, 
                           mintemp=np.ma.min(self.img2), 
                           maxtemp=np.ma.max(self.img2),
                           solartime=self.params.solarTime2))
        lines.append(
            imageDiff.format(mindiff=np.ma.min(self.diff), 
                           maxdiff=np.ma.max(self.diff)))
        lineImages = map(lambda x: renderText(x), lines)
        
        img = img.resize((self.outputWidth, self.outputHeight), Image.BILINEAR)
        res = yImages([img] + lineImages)
        return res

        

    def getOutputResult(self):
        """This produces a nice output image image with the output dust-depth
 image, the output TI, and the invalid mask image."""
        imgs = []

        img1 = self.decorateImage(sstretch(self.avgTI))

        imgs.append(gArrToImage(img1))
        imgs.append(self.colorizeWithScaleBar(self.tempError, unit='K'))
        imgs.append(self.colorizeWithScaleBar(self.output, unit='m'))

        # This returns an image that doesn't need stretching and is RGB,
        # so it gets handled specially.
        #iv = self.getInvalidValues().filled(0)
        #(h, w, _) = iv.shape
        #ival = Image.frombuffer("RGB", (w, h), iv, "raw", "RGB", 0, 1)
        #img = xImages([imgs[0], ival, imgs[1]])
        img = xImages(imgs)

        generalInfo = "Scalebar is 10 km, {lon:3.2f} E {lat:2.2f}"
        caption = "Thermal inertia: min {minti:3.1f}, max {maxti:3.1f}, avg {avgti:3.1f}"
        errcap = "Temperature error is: min {minerr:3.2f}, max {maxerr:3.2f}, avg {avgerr:3.2f}"
        imageTI = "Depth of low-TI layer: min {mindepth:1.3f} meters, max {maxdepth:1.3f} meters"
        lines = []
        lines.append(generalInfo.format(lon=self.params.lon, lat=self.params.lat))
        lines.append(
            caption.format(minti=np.ma.min(self.avgTI), 
                           maxti=np.ma.max(self.avgTI),
                           avgti=np.ma.average(self.avgTI)))
        lines.append(
            errcap.format(minerr=np.ma.min(self.tempError),
                          maxerr=np.ma.max(self.tempError),
                          avgerr=np.ma.average(self.tempError)))
        #lines.append(
        #    caption.format(idnum=self.params.imgid2, ls=self.params.ls2, 
        #                   mintemp=np.ma.min(self.img2), 
        #                   maxtemp=np.ma.max(self.img2)))
        lines.append(
            imageTI.format(mindepth=np.ma.min(self.output), 
                           maxdepth=np.ma.max(self.output)))
        lines = map(lambda x: renderText(x), lines)
        
        img = img.resize((self.outputWidth, self.outputHeight), Image.BILINEAR)
        res = yImages([img] + lines)
        return res

    def getGraphResult(self):
        """This produces an image with graphs and such on it."""
        dim = (self.outputWidth / 2, self.outputHeight)
        dim2 = (self.outputWidth / 2, self.outputHeight / 2)
        f1 = self.getDepthTIPlot()
        f2 = self.getImageHistogramPlot()
        f3 = self.getYearlyTempPlot()
        a = figToImage(f1, dim2)
        b = figToImage(f2, dim)
        c = figToImage(f3, dim2)
        subimg1 = yImages([a, c])
        #f1.close()
        #f2.close()
        img = xImages([subimg1, b])
        return img

    def writeResults(self, prefix=""):
        """Write out all results to files."""
        print "Generating pretty pictures."
        src = self.getInputResult()
        output = self.getOutputResult()
        graphs = self.getGraphResult()
        label = "{0}-{1}-{2}".format(prefix, self.params.imgid1, self.params.imgid2)
        # Simple and keeps the files in the order we'll want to look at them in, at least.
        graphsFile = "{0}-a.png".format(label)
        srcFile = "{0}-b.png".format(label)
        outputFile = "{0}-c.png".format(label)
        print "Writing results."
        src.save(srcFile)
        output.save(outputFile)
        graphs.save(graphsFile)
        saveAsIsis("{0}.cub".format(label), self.output.astype('float32'), dtype=gdal.GDT_Float32)
        saveAsIsis("{0}-TI1.cub".format(label), self.s1TI.astype('float32'), dtype=gdal.GDT_Float32)
        saveAsIsis("{0}-TI2.cub".format(label), self.s2TI.astype('float32'), dtype=gdal.GDT_Float32)



#a = gdalToImageAtRect(loadPDS("I17904012.cub"))
#b = gdalToImageAtRect(loadPDS("I18216009.cub"))

#runp = RunParams("I17904012.cub", "I18216009.cub", 200, 0.3, 0.2, 2000, -66.7, 35.96)
#runp = RunParams("I14393035.cub", "I14705021.cub", 200, 0.3, 0.2, -4130, 68.3, 132.0)
#runp = RunParams("I03388003.cub", "I07882014.cub", 200, 0.3, 0.2, -1715.0, 43.3, 35.25)
#runp = RunParams("I29268005-trimmed.cub", "I06380010-trimmed.cub", 200, 0.25, 0.23, -2415.0, 10.0, 156.0)
#runp = RunParams("I04133003-trimmed.cub", "I07129007-trimmed.cub", 200, 0.25, 0.23, -2415.0, 10.0, 156.0)
#runp = RunParams("I26136014-trimmed.cub", "I15728014-trimmed.cub", 200, 0.25, 0.23, 1415.0, -63.0, 156.0)

#run = Run(runp)
#run.doSetup()
#run.doTemp2Depth()
#run.doTemp2DepthNew()
#run.doTIImages()
# for i in range(-80, 80, 10):
#     print "Running with TI {0}".format(i)
#     runp = RunParams("I17904012", "I18216009", 200, 0.3, 0.2, 2000, i, 35.96)
#     run.params = runp
#     run.doTemp2Depth()
#     outname = "/mars/u/sheath/output1-lat{0:03d}.png".format(i)
#     run.savePlot(outname)
    

#run = Run(runp)
#run.doSetup()
#run.doTemp2Depth()
#run.showSim()



#run.writeResults(prefix='bar')
