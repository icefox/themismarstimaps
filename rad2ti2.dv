#!/usr/local/bin/davinci -fq
verbose=0


####################
# Settings
####################


opt={\
band=9,\
file_list="file_list",\
start=1,\
delim=" ",\
suffix=".cub",\
cubdir="./",\
\
dust=0.25,\
usetau=1,\
ltrange=0.30,\
lnrange=0.30,\
lsrange=32.0,\
\
minti = 20.0,\
maxti = 3000.0,\
interval=10.0,\
sfact=.04,\
sinc=3.0,\
\
cll=0,\
ignore=0,\
verbose=0,\
start=1\
};


############
# Available options to set above
############
# band-		Choose band for analysis (default=9)
# file_list-	Input list of files and  corresponding lat and lon coordinates (ex. I####### lat lon)
# start-	Choose starting line in file_list (default=1)
# delim-	Delimeter that seperates cube name, lat, and lon (default=" ")
# suffix-	Your cube suffixes (what comes after I#######) (default=".cub")
# cubdir-	Directory where cubes are located (Deault="./")
#
# dust-		Specify total dust opacity (tau) value for krc_bin if usetau option is not flagged.
# usetau-	Flag to determine averaged total dust opacity from TES (default=1). Will check for pre-existing vanilla files with tot_dust.
# ltrange-	Plus/minus range around center latitude used when retrieve TES dust opacity (e.g. 0.25)
# lnrange-	Same as above, around center longitude (e.g. 0.25)
# lsrange-	Plus/minus solar longitude of THEMIS image for TES dust opacity retrieval (e.g. 20.0)	
#
# minti-	Starting thermal inertia value for look-up table (default=1.0)
# maxti-	Largest ti value for look-up table (default=2200.0)
# interval-	Thermal inertia interval in look-up table (interval=10.0)
# sfact-	Scaling factor for KRC_bin TI for loop: ti+=ti*sfact+sinc (default=.05)
# sinc-		Scaling increment. See above sfact. (default=75)
#
# cll-		Flag to use center lat and lon for cube (only works if not cropped). NOTE:If using this option, file_list should contain only file names, no lat or lon
# ignore-	Ignore specific temperature value when determining TI (default=0)
# verbose-	Set the verbosity level (default=0)
#############



verbose=opt.verbose
#load standard davinci library
source("/themis/lib/dav_lib/library.dvrc")

#Load bilinear interpolation algorithm
verbose=0
load_module("cse")

# option preparation and assignment
opt=getopt($ARGV,opt)

#print the default options
printf("\nSelected options: \n")
opt_usage(opt)
printf("\n")
verbose=opt.verbose


# initialize isis3
isis3setup()


# read the cube list, print to user
if(fexists(opt.file_list)) {
  list = read_lines(opt.file_list)
  if (opt.cll!=1){
    split=strsplit(list,delim=opt.delim)
  }
  printf("Reading file_list \n")
}else {
  printf("File '%s' does not exist \n",opt.file_list)
  exit()
}


######Loop to go through the image IDs listed in file_list
##Load first file
##Get lat lon from file_list, or get center lat and lon if cll=1
for (i = opt.start ; i <= length(list) ; i+=1) {
  if(opt.cll!=1){
    file = split[i][,1]
    lt = atof(split[i][,2])
    ln = atof(split[i][,3])
    a=load_pds(opt.cubdir+file+opt.suffix)
    printf("\n Processing %s now \n",file)
  }else {
    file=list[,i]
    printf("\nProcessing %s now \n",file)
    a=load_pds(opt.cubdir+file+opt.suffix)
    minlat=a.IsisCube.Mapping.MinimumLatitude
    maxlat=a.IsisCube.Mapping.MaximumLatitude
    lt = (maxlat+minlat)/2.0			#center latitude of cube
    minlon=a.IsisCube.Mapping.MinimumLongitude
    maxlon=a.IsisCube.Mapping.MaximumLongitude
    ln=(maxlon+minlon)/2.0			#center longitude of cube
    printf("Using center lat and lon. \n")
    if(lat == 0.0) {
	printf("Center lat/lon not correctly retrieved.\n Is your image cropped? \n  If so, you must specify a lat/lon in file_list \n")
	exit()
    }
  }

  if(fexists(opt.cubdir+file+opt.suffix)!=1){
       prinft("Someting is wrong with your file list \n")
       exit()
  }
  printf("lat is %.2f \n",lt)
  printf("lon is %.2f \n",ln)
  

  ###
  ##Get local hour and solar longitude from id info
  ###
  info=idinfo(file)
  hr=info.local_solar_time
  printf("Local solar time is %.2f \n", hr)
  lsubs=info.solar_longitude
  #lsubs=info.solar_longitude*(80.0/360.0)
  printf("Solar longitude is %.2f \n",lsubs)



  ###
  ##Get average atmospheric dust opacity
  ###
  if(opt.usetau==1){

    if(fexists(file+"tau.van")){			#load pre-existing vanilla file
      printf("Reading tau_dust vanilla file \n")
    }else {						#query tes database for vanilla files within specified lat/lon/lsubs range
      maxlt=lt+opt.ltrange
      minlt=lt-opt.ltrange
      maxln=ln+opt.lnrange
      minln=ln-opt.lnrange
      maxls=lsubs+opt.lsrange
      if(maxls > 359.99){
      	maxls=maxls-360.0
      }
      minls=lsubs-opt.lsrange
      if(minls < 0){
      	minls=minls+360.0
      }

      printf("\n\nSubmitting vanilla request for TES total dust: \nlat %.2f:%.2f \nlon %.2f:%.2f \nlsubs %.2f:%.2f\n",minlt,maxlt,minln,maxln,minls,maxls)
      syscall(sprintf("vanilla /mars/readonly/tes/mgst/data/mgst_tabs/dataset.lst -fields 'tot_dust' -select 'ock 1583 7000 target_temp 220.0 310.0 scan_len 1 1 incidence 0.0 80.0 det_mask 7 7 spectral_mask 0 0 emission 0 5.0 latitude %f %f longitude_00 %f %f heliocentric_lon %f %f' > ./%stau.van",minlt,maxlt,minln,maxln,minls,maxls,file))
      printf("Vanilla file saved.\nLoading tot_dust. \n")
    }

    van=load_vanilla(file+"tau.van")
    printf("%i TES pixels retrieved for dust opacity average.\n.\n.\n.\n.\n",length(van.tot_dust))


    if(length(van.tot_dust)<3){
      printf("Vanilla query did not return a sufficient number of TES pixels.\nTry loosening your constraints.\n\n")
      syscall(sprintf("rm %stau.van",file))		#Remove empty vanilla file.
      answer=pause("Use predefined dust opacity? Enter 1 or 0 \n")
      if(atoi(answer)==1){				#Use default specified value if user chooses to do so. Or quit. 
      	dust=opt.dust
      	printf("Using user-defined total dust opacity %.3f\n.\n.\n.\n.\n.\n.\n.\n",dust)
      }else{
      	exit()
      }
    }else{    
      van.tot_dust[where(van.tot_dust > 0.32)]=0	#exclude TES pixels with dust storm values
      dust=(avg(van.tot_dust,ignore=0))*2.0             #IR opacity*visible/9-um extinction opacity ratio (Fergason et al., 2006; Clancy et al., 1995)
      printf("Average dust opacity is %.3f\n\n.\n.\n.\n.\n.\n.\n.\n",dust)
    }


  }else {
    dust=opt.dust					#Use default specified value if usetau option not flagged
    printf("Using user-defined total dust opacity.\n.\n.\n.\n.\n.\n.\n.\n ")
  }



  #####
  #Create look-up table for temperature to thermal inertia
  #Run KRC for user defined series of thermal inertias at 
  #####
  rows=200 #number of temperature calculations that will be done
  table=0*create(2,rows,format=float,org=bsq)	#blank array, columns for TI and temp
  k=1
  #in loop, j is thermal inertia, k is row number in table array
  printf("Running krc_bin.\n.\n.\n.\n.\n.\n.\n")
  for (j=opt.minti; j <= opt.maxti ; j+=((opt.sfact*j)+opt.sinc)) {
    table[1,k]=j				#thermal inertia saved to table column 1
    temps=krc_bin(lat=lt, lon=ln, ti=j, tau=dust)
    tk=translate(temps.data.tk,z,y)						#Increases tk array on all four sizes for 
    tk=cat(tk[,80],tk,axis=y)							#interpolation (to loop hour 24 back to 1, ls 355.5 back to 0)
    tk=cat(tk[24,],tk,axis=x)
    tk=cat(tk,tk[,2],axis=y)
    tk=cat(tk,tk[2,],axis=x)
    table[2,k] = cse.interp2d(tk,hr,lsubs,0,1,-4.5,4.5)	#temperature saved to table column 2
    #table[2,k] = temps.data.tk[int(round(hr)),1,int(round(lsubs))]             #without interpolation
    k+=1
  }
  #write(table, "unsorted.csv", type=csv, force=1)
  k=k-1


  #mp=minpos(table[2,],ignore=0)
  #mp=mp[2]
  #finaltable=table[,mp:k] #removes lower temperature values in beginning and trailing zeroes at end
  finaltable=table[,1:k] #removes trailing zeroes at end of table
  write(finaltable, file+"table.csv", type=csv, force=1)
  
  printf("Lookup table for %s saved.\n",file)
  printf("average table temp is %f\n",avg(finaltable[2,]))
  printf("average table ti is %f\n",avg(finaltable[1,]))
  



  ######
  #Radiance in ISIS cube with thermal inertias from look-up table
  ######
  tb=thm.rad2tb(a.cube,opt.band)  #radiance to brightness temperature
  itp=interp(finaltable[1,],finaltable[2,],tb,ignore=opt.ignore)
  a.cube=itp
  a.cube[where(a.cube<0)]=0
  #save pds-loaded cube, now with thermal inertia instead of radiance
  verbose=1
  write(a, file+"ti.cub", type=isis3, force=1) 
  verbose=opt.verbose
  a=0
  tb=0
  table=0
  finaltable=0
  lt=0
  ln=0
  hr=0
  lsubs=0
}