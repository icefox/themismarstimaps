# Fantastically general stuff.

from collections import namedtuple
import StringIO
import time
import os
import urllib
import subprocess

# Magic to set matplotlib to not complain about an X display
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import Image
import ImageFont
import ImageDraw
import numpy as np
import scipy.weave as weave
import scipy.interpolate as ip


from osgeo import gdal
from osgeo import ogr
from osgeo import osr
from osgeo import gdal_array
from osgeo import gdalconst as gdc

# Mosaic functions
# Thanks mainly to Drake Wilson.
class Point(namedtuple('Point', 'x y')):
    __slots__ = []

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)
    def __sub__(self, other):
        return Point(self.x - other.x, self.y - other.y)

class TLRect(namedtuple('TLRect', 'leftX topY rightX bottomY')):
    """First point is the top-left corner, second is the bottom right.
Always.
As God intended.
"""
    __slots__ = []
    @staticmethod
    def of_corner(c, w, h):
        return TLRect(c.x, c.y, c.x+w, c.y-h)

    @staticmethod
    def of_corners(topLeft, bottomRight):
        return TLRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y)

    def top_left(self):
        return Point(self.leftX, self.topY)
    def bottom_right(self):
        return Point(self.rightX, self.bottomY)

    def shape(self):
        w = self.width()
        h = self.height() 
        # Numpy does row-major ordering
        return (h,w)

    def width(self):
        return self.rightX - self.leftX

    def height(self):
        return self.topY - self.bottomY

    def size(self):
        return (self.width(), self.height())

    def __add__(self, offset):
        return TLRect.of_corners(self.top_left() + offset, 
                                 self.bottom_right() + offset)

    def __sub__(self, offset):
        return TLRect.of_corners(self.top_left() - offset, 
                                     self.bottom_right() - offset)

    def intersect(self, other):
        lowX = max(self.leftX, other.leftX)
        highX = min(self.rightX, other.rightX)
        lowY = min(self.topY, other.topY)
        highY = max(self.bottomY, other.bottomY)
        if lowX > highX or lowY < highY:
            raise RuntimeError("No intersection!")
        return TLRect(lowX, lowY, highX, highY)

    def getGeoTransform(self):
        """Turns the rect into a GDAL geotransform.
Since these are always north-is-up THEMIS images at 100 meters/pixel, this is easy.
For the format of this, see http://www.gdal.org/gdal_datamodel.html
"""
        gt0 = self.leftX * 100.0
        gt3 = self.topY * 100.0
        gt2 = 0.0
        gt4 = 0.0
        gt1 = 100.0
        gt5 = -100.0
        return (gt0, gt1, gt2, gt3, gt4, gt5)


class ImageAtRect(namedtuple('ImageAtRect', 'rect image')):
    """A rectangle and an image.
The rect defines the location and shape of the image inside a 
larger coordinate system.  Though currently the assumption is
that the coordinates in the rect and the image have the same
scale; 1 cell = 1 pixel.
This is basically used for mongling projected images; I use 
it for cutting out overlapping images, but with some help it
could be used for mosaicking too."""
    __slots__ = []

    @staticmethod
    def zero(rect):
        """Creates an ImageAtRect at the given coordinates.
Creates the required image and fills it with zeros."""
        s = rect.shape()
        image = np.zeros(s)
        return ImageAtRect(rect, image)

    def image_insert(self, other):
	"""Given an additional source image, overwrite any 
overlapping areas in this image using pixels from the source 
that have the same absolute position."""
        intersection = self.rect.intersect(other.rect)
        selfXStart = intersection.leftX - self.rect.leftX
        selfYStart = self.rect.topY - intersection.topY
        selfXEnd = selfXStart + intersection.width()
        selfYEnd = selfYStart + intersection.height()

        otherXStart = intersection.leftX - other.rect.leftX
        otherYStart = other.rect.topY - intersection.topY
        otherXEnd = otherXStart + intersection.width()
        otherYEnd = otherYStart + intersection.height()

        aslice = (slice(selfYStart, selfYEnd), slice(selfXStart, selfXEnd))
        bslice = (slice(otherYStart, otherYEnd), slice(otherXStart, otherXEnd))
        self.image[aslice] = other.image[bslice]
   

# This assumes a THEMIS image, 100 m/pixel
def gdalToImageAtRect(gdalimage):
    """Turns a THEMIS GDAL image into an ImageAtRect."""
    gt = gdalimage.GetGeoTransform()

    if gt == None:
        raise RuntimeException("gdal image does not have a geotransform!")

    (x, _, _, y, _, _) = gt
    x = x / 100.0
    y = y / 100.0
    #r = Rect.of_upper_left_corner(x, y, gdalimage.RasterXSize, gdalimage.RasterYSize)
    r = TLRect.of_corner(Point(x, y), gdalimage.RasterXSize, gdalimage.RasterYSize)
    dat = gdalimage.GetRasterBand(1).ReadAsArray()
    return ImageAtRect(r, dat)


def loadPDS(filename):
    """Simple wrapper to load the given file and return a GDAL handle."""
    f = gdal.Open(filename, gdc.GA_ReadOnly)
    # The NFS server is kinda flaky sometimes, so put in a retry.
    if f == None:
        time.sleep(0.5)
        f = gdal.Open(filename, gdc.GA_ReadOnly)
    # If that didn't work, well, shit.
    if f == None:
        errmsg = "Could not open file {0} in path {1}".format(filename, os.getcwd())
        raise RuntimeError(errmsg)
    return f

def gdalSave(filename, arr, driver, dtype=gdal.GDT_Float32, geotransform=None):
    """General function to save an array as an image using GDAL."""
    (h, w) = arr.shape
    dst = driver.Create(filename, w, h, 1, dtype)
    try:
        dst.GetRasterBand(1).WriteArray(arr)
        if geotransform != None:
            dst.SetGeoTransform(geotransform)
    finally:
        # Null the variable to destroy the image handle, which then writes the result
        # to disk.
        dst = None

def gArrToImage(arr):
    """Turns a numpy greyscale array into an Image."""
    arr = np.ma.filled(arr, 0)
    arr = arr.astype(np.uint8)
    (h, w) = arr.shape
    img = Image.frombuffer("L", (w, h), arr, "raw", "L", 0, 1)
    return img

def rgbArrToImage(arr):
    """Turns a numpy 3-channel RGB array into an Image."""
    arr = np.ma.filled(arr, 0)
    arr = arr.astype(np.uint8)
    (y,x,z) = arr.shape
    if z != 3:
        raise RuntimeError("Needs a 3-band image to convert to Image.")
    i = Image.frombuffer('RGB', (x,y), arr, 'raw', 'RGB', 0, 1)
    #i = i.transpose(Image.FLIP_TOP_BOTTOM)
    return i


    (h, w, z) = arr.shape
    img = Image.frombuffer("RGB", (w, h), arr, "raw", "RGB", 0, 1)
    return img

def saveAsGeotiff(filename, arr, dtype=gdal.GDT_Float32):
    """General function to save an array as a GeoTIFF image.
Doesn't include any actual geographical information or anything 
though, just raw data."""
    driver = gdal.GetDriverByName("gtiff")
    gdalSave(filename, arr, driver, dtype)

def saveAsFits(filename, arr, dtype=gdal.GDT_Float32, geotransform=None):
    """General function to save an array as a FITS image.
ISIS3 seems to have issues importing geotiffs, for some reason."""
    driver = gdal.GetDriverByName("fits")
    gdalSave(filename, arr, driver, dtype, geotransform=geotransform)

def saveAsIsis(filename, arr, dtype=gdal.GDT_Float32, geotransform=None):
    """Saves an array as an ISIS cube image.
...by saving it as a FITS image and then calling fits2isis.
Source array must be a float32.
XXX: Also... fits2isis destroys any georeferencing info in the fits file, dammit."""
    tmpname = filename + ".fits"
    saveAsFits(tmpname, arr, dtype=gdal.GDT_Float32, geotransform=geotransform)
    command = ["fits2isis", "from={0}".format(tmpname), "to={0}".format(filename)]
    proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    retval = proc.wait()
    print proc.stdout.read()
    if retval != 0:
        print proc.stderr.read()
        raise RuntimeException("saveAsIsis(): fits2isis failed for some reason")
    os.remove(tmpname)

def saveAsPng(arr, filename):
    """General function to save an array as an 8-bit greyscale
PNG image."""
    # The GDAL PNG driver currently installed does not
    # seem to have the capability to create PNG images raw.
    # It needs to use CreateCopy() from an example PNG image.
    # Weeeeird.  And lame.
    # So, in lieu of always having an example image around to copy from...
    # We use PIL.
    # ...we could also read to a numpy array and use scipy.misc.pilutil.toimage
    # Ho well.
    arr = arr.astype(np.uint8)
    (y,x) = arr.shape
    i = Image.frombuffer('L', (x,y), arr, 'raw', 'L', 0, 1)
    # I think Image believes (0,0) is the LOWER-left corner.
    i2 = i.transpose(Image.FLIP_TOP_BOTTOM)
    i2.save(filename, 'PNG')

def display1(arr):
    """Takes a (masked) 1-band numpy array and displays it."""
    arr = arr.astype(np.uint8)
    (y,x) = arr.shape
    i = Image.frombuffer('L', (x,y), arr, 'raw', 'L', 0, 1)
    #i = i.transpose(Image.FLIP_TOP_BOTTOM)
    i.show()

def display3(arr):
    """Takes a (masked) 3-band numpy array and displays it."""
    arr = arr.astype(np.uint8)
    (y,x,z) = arr.shape
    if z != 3:
        raise RuntimeError("Needs a 3-band image to display.")
    i = Image.frombuffer('RGB', (x,y), arr, 'raw', 'RGB', 0, 1)
    #i = i.transpose(Image.FLIP_TOP_BOTTOM)
    i.show()

def display(arr):
    """Takes a (masked) numpy array with 1 or 3 bands and displays it."""
    arr = np.ma.filled(arr, 0)
    shape = arr.shape
    if len(arr.shape) == 3:
        display3(arr)
    else:
        display1(arr)


def sdisplay(arr):
    """Takes a (masked) numpy array, does a sigma-stretch to it, and 
displays it."""
    s = sstretch(arr)
    display(s)

def sstretch(dat, variance=40.0):
    """Sigma-stretches an arbitrary 2d numpy (masked) array to be a 
2d array of bytes."""
    average = np.ma.average(dat)
    stddev = np.ma.std(dat)
    dat2 = ((dat-average)*(variance/stddev) + 127)
    dat2[dat2 < 0.0] = 0.0
    dat2[dat2 > 255.0] = 255.0
    return np.ma.asarray(dat2, dtype=np.uint8)


def combineMask(arr1, arr2, ignore=-32768.0):
    """Creates a pair of masked arrays using the given ignore value,
then sets the mask for each to be the sum of the two and returns
them."""
    m1 = np.ma.masked_values(arr1, ignore)
    m2 = np.ma.masked_values(arr2, ignore)
    mask = np.logical_or(m1.mask, m2.mask)
    m1.mask = mask
    m2.mask = mask
    return (m1, m2)

def trimEdges(arr):
    """Takes a masked array, returns a new array which has all the 
entirely-masked rows and columns around the edges are removed.
"""

    # Vector boolean fun times!
    p = arr.mask
    columns = np.all(p, axis=0).tolist()
    rows = np.all(p, axis=1).tolist()
    if False in columns:
        leftStart = columns.index(False)
    else:
        leftStart = 0
    
    if False in rows:
        topStart = rows.index(False)
    else:
        topStart = 0

    # I'm sure there's a more elegant way of doing this,
    # but I dunno what it is.
    # Arrays don't seem to have rindex().
    columns.reverse()
    if False in columns:
        rightStart = len(columns) - columns.index(False) - 1
    else:
        rightStart = 0
    rows.reverse()
    if False in rows:
        bottomStart = len(rows) - rows.index(False) - 1
    else:
        bottomStart = 0

    return arr[topStart:bottomStart, leftStart:rightStart]

def themisRad2TB(arr, band, radfile="/mars/u/sheath/dat/temp_rad_v4.cub"):
    """Convert THEMIS radiance to calibrated brightness-temperature 
for the given band (numbered 1-11)"""
    rad = gdal.Open(radfile, gdc.GA_ReadOnly)
    rad = rad.GetRasterBand(1).ReadAsArray()
    # A table of temperatures
    # We create a NEW array here, because if we just take a slice out of rads
    # then things get cattywompus in C++ land.
    temps = np.array(rad[:,0], copy=True)
    # And the radiances conversions for the band we want
    rads = np.array(rad[:, band], copy=True)
    radsSize = rads.shape[0]
    
    m = np.zeros(rads.shape)
    b = np.zeros(rads.shape)

    # Calculate tables of slopes and intercepts, for
    # a linear interpolation.
    for i in range(1, len(rads)):
        m[i-1] = (temps[i] - temps[i - 1]) / (rads[i] - rads[i-1])
        b[i-1] = temps[i-1] - (m[i-1] * rads[i-1])

    outArray = np.ones(arr.shape)
    (arry, arrx) = arr.shape

    # Masked arrays in weave.inline are mysterious.
    dat = np.ma.filled(arr, fill_value=-32768.0)
    code = """
#line 376 "imageProcessing.py"
int i = 0;
int pixels = 0;
// For each pixel...
for(i = 0; i < arrx; i++) {
   int j = 0;
   for(j = 0; j < arry; j++) {
      int idx = i * arry + j;
      double val = dat[idx];
      // Check for invalid values
      if(val == -32768.0) {
         outArray[idx] = 0.0;
      } else {
         // Binary search through the rads array, looking for
         // values that match the radiance value.
         int pt1 = 0;
         int pt2 = radsSize;
         int mid = 0;
         while((pt2 - pt1) > 1) {
            mid = (pt1 + pt2) / 2;
            double m = rads[mid];
            //printf("Checking %f against %f with pt1 %d, pt2 %d, mid %d\\n", val, m, pt1, pt2, mid);
            if(val > m) pt1 = mid;
            if(val < m) pt2 = mid;
            if(val == m) pt1 = pt2 = mid;
         }
         //printf("Found solution: val %f, rads %f, pt1 %d, pt2 %d, mid %d\\n", val, rads[mid], pt1, pt2, mid);

         double res = -5.0;
         if(temps[pt2] == temps[pt1]) {
            res = temps[pt1];
         }
         else { 
            res = m[pt1] * val + b[pt1];
         }

         outArray[idx] = res;
         pixels++;
      }
   }
}
//printf("Processed %d pixels\\n", pixels);
"""
    weave.inline(code, ['arrx', 'arry', 'dat', 'outArray', 'radsSize', 'rads', 'temps', 'm', 'b'])
    # Re-mask the result
    return np.ma.array(outArray, mask=arr.mask)
    

# I wrote this on a whim, but it actually compares quite well to the 
# weave.inline version.
# One problem is that it crunches the values down to 32-bit floats, but that 
# should be fixable.
def themisRad2TBSciPy(arr, band, radfile="/mars/u/sheath/dat/temp_rad_v4.cub"):
    """Sorta experimental; just use themisRad2TB()."""
    rad = gdal.Open(radfile, gdc.GA_ReadOnly)
    rad = rad.GetRasterBand(1).ReadAsArray()
    # A table of temperatures
    temps = rad[:,0]
    # And the radiances conversions for the band we want
    rads = rad[:, band]

    f = ip.interp1d(rads, temps, kind='linear', bounds_error=False, fill_value=0.0)
    calArr = f(arr)
    result = np.ma.masked_values(calArr, 0.0)
    return result

# Keeping this version around for reference.
def themisRad2TBSlow(arr, band, radfile="/mars/u/sheath/dat/temp_rad_v4.cub"):
    """Convert THEMIS radiance to calibrated brightness-temperature for 
the given band (numbered 1-11).
Warning!  This can take hours for large images!  This function is 
mainly here for algorithm reference."""
    rad = gdal.Open(radfile, gdc.GA_ReadOnly)
    rad = rad.GetRasterBand(1).ReadAsArray()
    # A table of temperatures
    temps = rad[:,0]
    # And the radiances conversions for the band we want
    rads = rad[:, band]
    
    m = np.zeros(rads.shape)
    b = np.zeros(rads.shape)

    # Calculate tables of slopes and intercepts, for
    # a linear interpolation.
    for i in range(1, len(rads)):
        m[i-1] = (temps[i] - temps[i - 1]) / (rads[i] - rads[i-1])
        b[i-1] = temps[i-1] - (m[i-1] * rads[i-1])

    global count
    count = 0
    def calPixel(pix):
        global count
        count += 1
        if (count % 1000) == 0: print count
        # maxEm is always 1 in the davinci code, sooooo...?
        #val = pix / maxEm
        val = pix
        # Binary search our way through the rads array, looking for the
        # values that either bracket or match the radiance value
        pt1 = np.searchsorted(rads, val) - 1        
        #if temps[pt2] == temps[pt1]: 
        #    return temps[pt1]
        #else: 
        return m[pt1] * val + b[pt1]

    ucal = np.frompyfunc(calPixel, 1, 1)
    return ucal(arr)

        
def getFig():
    fig = plt.figure()
    plot = fig.add_subplot(111)
    plot.plot([1,2,3,4,5])
    return fig


def figToImage(fig, size):
    """Takes a matplotlib Figure and turns it into an PIL Image."""
    (w,h) = size
    # INCHES?
    [wi, hi] = fig.get_size_inches()
    dpi = fig.get_dpi()
    reallynow = w / dpi
    areyouserious = h / dpi
    fig.set_size_inches((reallynow, areyouserious))
    # Yes, they are serious.

    sbuf = StringIO.StringIO()
    fig.savefig(sbuf, format='png', bbox_inches=0) # bbox_inches='tight')
    sbuf.seek(0)
    i = Image.open(sbuf)
    return i

def figToArray(fig):
    """This takes a matplotlib Figure object and returns a 3-band numpy
array representing the image derived from it."""
    # This is TOTALLY STUPID.
    # But I can't figure out a better way that actually WORKS without
    # calling freakin' fig.show() somewhere in there.
    # and fig.show is weird and horrifically stateful and just kind of
    # bananas.
    # Not that this is less bananas...
    i = figToImage(fig)
    # Make sure the data is RGB, instead of RGBA
    # Or something silly like ARGB
    i = i.convert('RGB')
    i = i.transpose(Image.FLIP_TOP_BOTTOM)
    (w, h) = i.size
    npdata = np.fromstring(i.tostring(), dtype=np.uint8)
    npdata = npdata.reshape((h, w, 3))
    return npdata

def xImages(images):
    """Takes a list of Image's and returns one with them all put together 
horizontally."""
    dims = map(lambda i: i.size, images)
    # This Magical Expression unzips the list-of-tuples
    [widths, heights] = zip(*dims)
    width = sum(widths)
    height = max(heights)
    dest = Image.new("RGB", (width,height))
    xoffset = 0
    for width, image in zip(widths, images):
        dest.paste(image, (xoffset, 0))
        xoffset += width
    return dest

def yImages(images):
    """Takes a list of Images and returns one with them all put together 
vertically."""
    dims = map(lambda i: i.size, images)
    [widths, heights] = zip(*dims)
    width = max(widths)
    height = sum(heights)
    dest = Image.new("RGB", (width,height))
    yoffset = 0
    for height, image in zip(heights, images):
        dest.paste(image, (0, yoffset))
        yoffset += height
    return dest

def renderText(text, bgcolor=(0,0,0), fgcolor=(255,255,255), 
               fontfile='/usr/share/fonts/liberation/LiberationSerif-Regular.ttf'):
    """Returns an Image with the given text rendered upon it."""
    # Liberation and dejavu are nice fonts...
    # DejaVu will probably have whatever greek letters you might need.
    font = ImageFont.truetype(fontfile, 15)
    (w, h) = font.getsize(text)
    img = Image.new("RGB", (w, h), bgcolor)
    draw = ImageDraw.Draw(img)
    draw.text((0, 0), text, fgcolor, font=font)
    return img


def colorize(img):
    """Takes a 1-band masked greyscale image and produces a 3-band color image
with colors:
0,0,255
0,255,0
255,255,0
255,128,0
255,0,0
"""
    colors = np.array([
        (0,0,255),
        (0,255,0),
        (255,255,0),
        (255,128,0),
        (255,0,0)
        ])
    minimum = np.ma.min(img)
    maximum = np.ma.max(img)
    increment = (maximum-minimum)  / float(len(colors) - 1)
    colorValues = np.arange(len(colors)) * increment + minimum
    #print zip(colorValues, colors)
    f = ip.interp1d(colorValues, colors.transpose(), bounds_error=False, fill_value=([0],[0],[0]))
    # Silly interpolate doesn't handle masked arrays
    imgUnmasked = np.ma.filled(img, minimum)
    res = f(imgUnmasked)
    # We get a shape of (3,x,y), we want (x,y,3)
    res = np.dstack((res[0], res[1], res[2]))
    resmask = np.dstack((img.mask, img.mask, img.mask))
    res = np.ma.array(res, mask=resmask)
    return res


def getJankyProductID(filename):
    """This is a semi-awful function which returns a string representing the THEMIS product ID
for the given file.

It is semi-awful because it doesn't actually parse anything, just trawls through the header 
for the ProductId line and yanks the ID out."""
    with open(filename, 'r') as f:
        for line in f.readlines():
            if '=' not in line:
                continue
            #else:
                #print "Line is", line
            keyval = line.split('=')
            key = keyval[0].strip()
            val = keyval[1].strip()
            if 'ProductId' in key:
                return val[:9]
    # Didn't find anything.  D:
    raise KeyError


def themisDBQuery(query, header=False):
    """Accepts a MySQL query, runs it on the THEMIS database, returns the rows.  
Does not protect against SQL injection attacks though.  :-P  
Or handle funny formatting with things like & in it, for that matter."""
    h = 0
    if header:
        h = 1
    # We pretend we're actually davinci.  Shhhhhh.
    dbUser = "davinci"
    dbPass = "davinci-team"
    server = "http://app-davinci.mars.asu.edu/Davinci/QueryTool?"
    querystr = "{server}user={user}&password={password}&queryString={query}&displayHeader={header}"
    q = querystr.format(server=server, user=dbUser, password=dbPass, query=query, header=h)
    print q

    u = urllib.urlopen(q)
    data = u.read()
    u.close()
    # Seems to just return a bunch of tab-separated string values.
    lines = data.splitlines()
    fields = map(lambda x: x.split('\t'), lines)
    print fields
    return fields

# Our version of the davinci idinfo() function.
# Gets fewer parameters though 'cause I don't need them all.
def themisIdInfo(tid):
    """Takes a THEMIS ID number, queries the THEMIS database, and returns a 
dict with misc. info about the ID.
Right now it only does IR images.  And it really only gets data from the 
first framelet anyway."""
    tid = tid[:9]
    if len(tid) != 9 or tid[0] != 'I':
        raise RuntimeError("Invalid THEMIS id number: {0}".format(tid))

    q = 'select file_id from qube where file_id = "{0}";'.format(tid)
    res = themisDBQuery(q)
    if len(res) == 0:
        raise RuntimeError("Themis ID number does not exist: {0}".format(tid))
    
    # Query IR parameters
    q1 = 'select surf_temp_min, surf_temp_max, surf_temp_avg from irfrmsci where irfrmsci.file_id = "{0}" and irfrmsci.framelet_id = 0;'.format(tid)
    # Query bands
    q2 = 'select header.band_bin_band_number from header, frmgeom where frmgeom.file_id = header.file_id and frmgeom.file_id = "{0}" and framelet_id = 0 and point_id = "CT";'.format(tid)
    # Query positional parameters
    q3 = 'select lat, lon, solar_longitude, local_solar_time from frmgeom where frmgeom.file_id = "{0}" and frmgeom.framelet_id = 0 and point_id = "CT";'.format(tid)

    res1 = themisDBQuery(q1)
    res2 = themisDBQuery(q2)
    res3 = themisDBQuery(q3)

    out = {}
    out['id'] = tid
    print res1
    # I guess some THEMIS images don't have valid temperatures...
    if res1[0][0] != 'null':
        out['min_btemp'] = float(res1[0][0])
        out['max_btemp'] = float(res1[0][1])
        out['avg_btemp'] = float(res1[0][2])
    else:
        out['min_btemp'] = 0.0
        out['max_btemp'] = 0.0
        out['avg_btemp'] = 0.0

    # Don't really care to parse this at the moment.
    out['bands'] = res2[0][0]

    out['lat'] = float(res3[0][0])
    out['lon'] = float(res3[0][1])
    out['ls'] = float(res3[0][2])
    out['solar_time'] = float(res3[0][3])

    return out



def getStats(arr):
    """Returns a tuple of statistics for the (masked) array given:
(min, max, average, std)"""
    mn = np.ma.min(arr)
    mx = np.ma.max(arr)
    av = np.ma.average(arr)
    st = np.ma.std(arr)
    return (mn, mx, av, st)
