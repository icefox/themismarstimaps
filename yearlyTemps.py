#!/usr/bin/env python
# This script runs a KRC simulation for a given location and parameters,
# then reads a bunch of given THEMIS images and produces average temperatures
# at the given Ls for each one.  Then it displays a graph of average temperature
# for each image, overlayed on KRC's predicted temperatures.


import sys
import os
import os.path

import matplotlib.pyplot as plt
import numpy as np

import krc
from imageProcessing import *


def writeGraph(runs, stats):
    print "Graphing..."
    # So 'runs' is an array of KRC outputs
    fig = plt.figure()
    plt.xlabel("Ls")
    plt.ylabel("Surface temperature (K)")
    plt.title("Temperature of given THEMIS images vs. KRC prediction")
    #print runs
    lines = []
    #for (r, color, lab) in zip(runs, colors, labels):


    # Now we have an array of all 

    for run in runs:
        # Each run at a different parameter variation...
        temps = run['tk']
        # This is an 80x24 array, so we want time of day 4 (5 am)
        # XXX: We actually want the time of day closest to the THEMIS images?
        tempsAtTime = temps[:,4]
        line = plt.plot(np.linspace(0, 360, 80), tempsAtTime, 'black')
        #rint tempsAtTime
        #print("Min {0:0.2f}, max {1:0.2f}, avg{2:0.2f}".format(np.min(tempsAtTime),
        #                                                       np.max(tempsAtTime),
        #                                                       np.average(tempsAtTime)
        #                                                       ))

        # 5 am is not EXACTLY when THEMIS flies over, generally.
        #tempsAtTime = temps[:,3]
        #line = plt.plot(np.linspace(0, 360, 80), tempsAtTime, 'blue')
        #tempsAtTime = temps[:,5]
        #line = plt.plot(np.linspace(0, 360, 80), tempsAtTime, 'red')

    for s in stats:
        (imageid, ls, mx, mn, av) = s
        plt.vlines(ls, mn, mx, linewidth=1.0, color='black')
        plt.plot(ls, mx, 'ro')
        plt.plot(ls, mn, 'bo')
        plt.plot(ls, av, 'go')
    #plt.legend(lines, map(lambda l: "{0}".format(l.get_label()), lines))
    plt.xlim((0, 360))
    plt.ylim((140, 250))
    plt.show()
    #i = figToImage(fig, (800, 600))
    #i.save('t1-3.png')



def readImage(filename):
    i = loadPDS(filename)
    ir = gdalToImageAtRect(i)
    im = np.ma.masked_values(ir.image, -32768.0)
    ic = themisRad2TB(im, 9)
    return ic

def imageStats(imagefile):
    image = readImage(imagefile)
    imageid = getJankyProductID(imagefile)
    thmData = themisIdInfo(imageid)
    ls = thmData['ls']
    mx = np.ma.max(image)
    mn = np.ma.min(image)
    av = np.ma.average(image)
    print "{0}: Ls {1:3.2f} max {2:03.2f} K min {3:03.2f} K avg {4:03.2f} K".format(imageid, ls, mx, mn, av)
    return (imageid, ls, mx, mn, av)

def usage():
    print "Usage: yearlyTemps.py ti tau albedo elevation lat image1 ..."
    sys.exit(0)

def main():
    print ' '.join(sys.argv)
    print os.getcwd()
    if len(sys.argv) < 6:
        usage()
    ti = float(sys.argv[1])
    tau = float(sys.argv[2])
    albedo = float(sys.argv[3])
    elevation = float(sys.argv[4]) / 1000.0
    lat = float(sys.argv[5])
    images = sys.argv[6:]

    print "Running KRC..."
    #layers = [1,5,9,13,17,21,25]
    layers = range(1, 29) 
    krcdats = [krc.runKRC(lat, 0, elev=elevation, alb=albedo, tau=tau, ti=ti,
                        slope=0, azimuth=0, layer=l)
               for l in layers]
    print "Reading THEMIS images..."
    stats = [imageStats(i) for i in images]
    print "Making graph..."
    writeGraph(krcdats, stats)

if __name__ == '__main__':
    main()
