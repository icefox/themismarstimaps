
# Magic to set matplotlib to not complain about an X display
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import math
import os
import shutil
import subprocess
import tempfile
import numpy as np
import scipy.interpolate as ip


# Make temp dir, copy files into it.

krcDir = "/themis/lib/dav_lib/krc"

# We need krc_part1.inp, krc_part2.inp, krc_part3.inp
# var_header.ascii, fake_out_krc_bin maybe.


# This function and the next parse a bin5 file.
# It is basically two chunks: a header, and a big fat
# array of data.  
def readBin5Header(fil):
    with open(fil, 'r') as f:
        dat = f.read()
        headerEnd = dat.index("C_END") + 5
        header = dat[:headerEnd]
        hlist = [i for i in header.split(' ') if i != '']
        numDims = int(hlist[0])
        dimsEnd = numDims + 1
        dims = [int(i) for i in hlist[1:dimsEnd]]
        dataType = int(hlist[dimsEnd])
        numElements = int(hlist[dimsEnd+1])
        data = dat[headerEnd:]
        return (numDims, dims, dataType, numElements, data)

def readBin5Body(header):
    (ndims, dims, dtype, numElts, data) = header
    if dtype != 4:
        raise RuntimeError("Expected datatype float (4), got {0}".format(dtype))

    # XXX: Check byte order!
    # Well as long as we only ever run on x86 then we don't need to...
    nums = np.fromstring(data, dtype=np.float32)

    # This seems to be common, and it also does not seem to pose a problem.
    # Scary.
    if len(nums) > numElts:
        #print "Warning: Expected {0} elements, got {1}.  Trimming to appropriate size.".format(len(nums), numElts)
        nums = nums[:numElts]

    # Okay, this line took about half an hour to write.
    # But I'm pretty sure it's the _correct_ line.
    arr = np.reshape(nums, dims, order='F')
    return arr

# This reads a bin5 file and returns an array with the contents.
def readBin5(filename):
    return readBin5Body(readBin5Header(filename))
    
# et is ephemeris time
# This is a function that returns Ls given an et, using an
# entirely emperical solution.
# This shit is bananas.
# Documented at http://davinci.mars.asu.edu/index.php?title=lsubs
def lsubs(et):
    const = 59340000.0
    scale = et / const * (2*math.pi) + 2
    
    # I can only pray that the parenthesis on this are correct,
    # because I don't really know what it should be.
    ls=(et+46090000) % const*360.0 / const + \
        math.sin(et*2*math.pi / const + 0.4)*10.75 + \
        6 * math.sin(-2*scale - 0.3) / 9.0 - \
        pow(math.sin(scale)+1, 4) / 9.0 - \
        3*et / 2000000000.0 - \
        8.75
    return ls % 360.0


def resampleToFixedLs(arr):
    """KRC outputs points separated by a fixed # of seconds.  But, we want points
separated by a fixed Ls.  Because Mars's orbit isn't circular, 1 degree of Ls is a
varying number of seconds, so we have to resample from seconds to Ls.
Note this only does a 1-d array at a time.
"""
    # First, generate our X axes: time-spaced Ls, which we are resampling from,
    # and evenly-spaced Ls, which we are resamplng to.  'arr', the input data, is
    # the Y axis.
    et = 13046146
    # This is in Earth days for some reason...                                                                        
    times = [et + (3600 * 24 * 8.58725 * i) for i in range(80)]
    diffSpacedLs = map(lsubs, times)
    # First value is not QUITE zero, which makes things screwy for the interpolation, so.
    diffSpacedLs[0] = 0
    evenlySpacedLs = np.linspace(0, 360, 80, endpoint=False)

    #fillVal = 0.0
    f = ip.interp1d(diffSpacedLs, arr, axis=0, kind='cubic')
    return f(evenlySpacedLs)
    #f = ip.interp1d(diffSpacedLs, arr, axis=0, kind='cubic', bounds_error=False, fill_value = 0.0)
    #plt.plot(diffSpacedLs, arr, 'o', evenlySpacedLs, f(evenlySpacedLs), '-')
    #plt.show()


# This reads a bin52 file and returns the actual data conveniently
def processBin52(filename):
    data = readBin5(filename)

    newdat = []
    for i in range(24):
        flatdata = [data[i,j,0,1:].flatten() for j in range(5)]
        datastack = np.vstack(flatdata)
        newdat.append(datastack)
    cube = np.dstack(newdat)


    tk = resampleToFixedLs(cube[0])
    tbol = resampleToFixedLs(cube[1])
    tatm = resampleToFixedLs(cube[2])
    down_vis = resampleToFixedLs(cube[3])
    down_ir = resampleToFixedLs(cube[4])
    return (tk, tbol, tatm, down_vis, down_ir)
    


def readLayers(filename):
    with open(filename, 'r') as f:
        dat = f.readlines()
        dat = map(lambda x: x.strip(), dat)
        #for x in dat: print x
        # GOTTA be a better way of doing this.  Oh well.
        i = 0
        for x in dat:
            if "LAYER" in x:
                break
            i += 1
        numberses = dat[i+1:i+30]
        numberses = map(lambda x: x.split(), numberses)
        nums = np.array(
            map(lambda x:
                    map(lambda y: float(y), x), numberses))
        # Layer thickness in skin depth and meters
        layerThickness = nums[:,1:3]
        # Layer center depth in skin depths and meters
        layerDepth = nums[:,3:5]

        layerTop = layerDepth - (layerThickness/2.0)

        return (layerThickness, layerDepth, layerTop)

# A rough translation of the davinci function krc_bin
# to at least generate input file appropriate for KRC,
# using a two-layer model and such for one point.
# See http://davinci.mars.asu.edu/extensions/Cedwards/getSourceFile.php?funcname=krc_bin#goto
# Careful study of the KRC documentation has made me conclude
# that this shit is bananas, so screw trying to understand what
# the hell is going on, let's just try to reproduce it...
# This is terrible, I know.  I'm sorry.  -_-
# ...but it is still waaaaaaaay better than the davinci version.  Ye gods.
def makeInput(outfile, lat, lon, elev, alb, tau, slope, azimuth, ti, layer):
    params = \
"""2 5 240 'N5' / seasons
2 12 161 'JDISK' /
1 42 8.5875 'DELJUL 1/80 year' /
1 3 {ti:.2f} 'TI' /
2 7 0 'IB' /
1 23 {slope:.2f} 'Slope' /
1 24 {azimuth:.2f} 'Azimuth' /
2 8 {layer} 'IC' /
2 17 52 'K4out' /
8 0 0 '{outfile}' /
1 17 {tau:.2f} 'TauD' /
1 1 {albedo:.2f} 'Albedo' /"""
    params = params.format(ti=ti, slope=slope, azimuth=azimuth, layer=layer,
                  outfile=outfile, tau=tau, albedo=alb)
# COND2's: 2.50 (initial), 1.35, 0.6, 0.15
    krcTemplate = \
""" 0 0 / KOLD: SEASON TO START WITH;  KEEP: CONTINUE SAVING DATA IN SAME DISK FILE
Input file for krc generated by PYTHON front end
    ALBEDO     EMISS   INERTIA     COND2     DENS2    PERIOD  SPECHEAT   DENSITY
       .15      1.00     200.0      2.50     2018.    1.0275      837.     1500.
      CABR    ABRAMP    ABRPHA    PTOTAL     FANON      TATM     TDEEP   SpHeat2
      0.18       .30      260.     689.7      .055      200.    178.00    1040.0
      TAUD     DUSTA    TAURAT     TWILI      ACR2      ARC3     SLOPE    SLOAZI
       0.2       .90      .501       1.0       0.5      0.10       0.0       90.
    TFROST    CFROST    AFROST     FEMIS       AF1       AF2    FROEXT      ARC6
     148.0   589944.       .65      1.00      0.54    0.0009      50.        0.0
      RLAY      FLAY     CONVF     DEPTH     DRSET       DDT       GGT     FDOWN
     1.150     .2000       1.5       0.0       0.0     .0020        .1       0.1
      DJUL    DELJUL  SOLARDEC       DAU      HLON    SOLCON      GRAV    Atm_Cp
   10322.4      4.07      00.0     1.465        .0     1368.     3.727      800.
        N1        N2        N3        N4        N5       N24        IB        IC
        29       384        10         1       120        24         1         0
     NRSET      NMHA      NRUN     JDISK     IDOWN       I14       I15     KPREF
        14        24         1       121         0        45        68         1
     K4OUT     spare     spare     spare                                     end
         0         0         0         0                                       0
    LP1    LP2    LP3    LP4    LP5    LP6 LPGLOB   LVFA   LVFT  debug
      F      T      F      F      F      F      F      F      T      F
  LPORB   LKEY    LSC LNOTIF  LOCAL  spare LPTAVE Prt.78 Prt.79   LONE
      T      F      F      F      T      F      F      T      F      F
LATITUDES: in 10F7.2  _____7 _____7 _____7 _____7 _____7 _____7 _____7
{lat:7.2f}
Elevations:in 10F7.2  _____7 _____7 _____7 _____7 _____7 _____7 _____7
{elev:7.2f}
  97-JAN-30 15:37:58 =RUNTIME.  IPLAN AND TC=   4.0 0.49000
   4.000000      0.4900000      0.8556749      0.3221672E-01   4.997373    
  0.9339862E-01   1.523693       10820.59       686.9796      0.9204510    
   5.539492       24.62278             0.      0.4093198             0.    
         0.             0.             0.       6.150894      0.4662405    
  0.4166342E-01  0.6202968       4.377076             0.             0.    
         0.             0.             0.             0.             0.    
         0.             0.             0.     -0.9491031E-01  0.8901852    
  0.4456034     -0.9056373      0.1086292     -0.4099036     -0.4132956    
 -0.4424591      0.7958748      0.9085325     -0.4166692     -0.3091195E-01
  0.4171058      0.9088128      0.9055913E-02  0.2431985E-01 -0.2112114E-01
  0.9994811     -0.3290646     -0.8544949     -0.4019393      0.9443074    
 -0.2977673     -0.1400645      0.3352761E-07  0.4256445     -0.9048904 
{params}
0/
0/
0/
"""
    krcInput = krcTemplate.format(lat=lat, elev=elev, params=params)
    return krcInput

KRCDIR = "/themis/lib/dav_lib/krc"

def runKRC(lat, lon, elev=0, alb=0.2, tau=0.3, slope=0, azimuth=0, ti=200, layer=0, keep=False):
    # Create temp dir
    tempdir = tempfile.mkdtemp()
    krcBinary = os.path.join(KRCDIR, "krc")

    infile = os.path.join(tempdir, 'krc.inp')
    prtfile = os.path.join(tempdir, 'krc.prt')
    outfile = os.path.join(tempdir, 'outdata.bin5')
    here = os.getcwd()
    # Create input file
    os.chdir(tempdir)
    inp = makeInput(outfile, lat, lon, elev, alb, tau, slope, azimuth, ti, layer)
    f = file(infile, 'w')
    f.write(inp)
    f.close()

    # Invoke KRC
    proc = subprocess.Popen(krcBinary, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    proc.stdin.write('krc.inp\nkrc.prt\n')
    proc.stdin.close()
    
    retval = proc.wait()
    if retval != 0:
        print("KRC exited with return value {0}; what does that even mean?!".format(retval))
        
    # Mongle output data
    (tk, tbol, tatm, down_vis, down_ir) = processBin52(outfile)

    # Shit now we need to parse layers
    (layerThickness, layerDepth, layerTop) = readLayers(prtfile)

    # So KRC is SUPPOSED to have a bunch of layers, each layer X skin depths
    # thicker than the previous one.  This appears to be the case, EXCEPT for
    # the layer where the TI changes, which is some apparently random number
    # that is consistent between layers but changes based on what the input TI is.
    # Which is crazy.  And incredibly inconvenient.
    #print "Thickness: ", layerThickness
    #print "Depth: ", layerDepth
    #print "Layer top: ", layerTop

    # Clean up
    os.chdir(here)
    if not keep:
        shutil.rmtree(tempdir)
    else:
        print("Keeping temp dir: " + tempdir)

    # I guess this is the most convenient way to return the data???
    ret = {
        'tk' : tk,
        'tbol' : tbol,
        'tatm' : tatm,
        'downVIS' : down_vis,
        'downIR' : down_ir,
        'layerThickness' : layerThickness,
        'layerDepth' : layerDepth,
        'layerTop' : layerTop,
        }
    
    return ret
