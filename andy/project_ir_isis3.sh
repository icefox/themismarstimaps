#!/bin/bash -f

############################################################################
############################################################################
##
## Projection Options
##
############################################################################
############################################################################

#Cluster Parameters
projlocation=1                          #Projection Processing Location: 0=local,1=MSFF Cluster
group="mos"                            #Identification for cluster jobs (Max 5 Characters)
queuename="low"                         #Cluster queue priority
mail="-M cluster-noise@mars.asu.edu"    #Direct the output mail (Use "" for none)
clustervar=""                           #cluster -l option (Use "-l nodes=1 -l qos=virtualize" for hpc)

#Projection Defaults
projection="SimpleCylindrical"          #The projection "SimpleCylindircal, Sinusoidal, PolarStereographic"
#projection="Sinusoidal"          #The projection "SimpleCylindircal, Sinusoidal, PolarStereographic"
clat=0.0                                #Center latitude *only used for PolarSterographic
# Set clon if not set...
# Center longitude
if [ $clon ]
   then clon=0.0
fi
slat=0.0                                #Latitude of true scale
lattype="PLANETOCENTRIC"                #Latitude type (PLANETOCENTRIC or PLANETOGRAPHIC)
lonsys="360"                            #Longitude domain (360=0:360 180=-180:180)
imgres=100                              #Image resolution (mpp)
interp="bilinear"                       #Interpolation type: "bilinear", "bicubic", "nearest"

#Cropping by Latitudes
minlat="--"                            #Minimum Latitude set to "--" for no cropping
maxlat="--"                             #Maximum Latitude

#Clean up and Band Selection Parameters
force=0                                 #Force the re-creation of products (force=1)
bandnum="9"                             #THEMIS band number
night_clean="1"                         #FLag to do advanced clean up of nighttime images
arc_wnr="0"                             #Flag to do autoradcorr and white noise removal
                                        #0 for deplaid/destreak
                                        #1 for deplaid/destreak/arc/wnr1
                                        #2 for deplaid/destreak/arc/wnr2
                                        #3 for deplaid w/o destreak
                                        #4 for deplaid/destreak/arc

###############################################################################
###############################################################################
##
## Map Projection Setup
##
###############################################################################
###############################################################################

source /themis/lib/mosaic/setupisis3beta.bashrc 



##
## Create a Map Template File
##

cp /themis/lib/mosaic/maptemplates/$projection.map $PWD

if [ $projection = "SimpleCylindrical" ]
then
    mapopt=`echo "projection=$projection clon=$clon targopt=user targetname=Mars lattype=$lattype resopt=mpp resolution=$imgres londom=$lonsys"`
fi

if [ $projection = "Sinusoidal" ]
then
    mapopt=`echo "projection=$projection clon=$clon targopt=user targetname=Mars lattype=$lattype resopt=mpp resolution=$imgres londom=$lonsys"`
fi

if [ $projection = "PolarStereographic" ]
then
    mapopt=`echo "projection=$projection clat=$clat clon=$clon targopt=user targetname=Mars lattype=$lattype resopt=mpp resolution=$imgres londom=$lonsys"`
fi 


##
## Use the ISIS3 Map Template to Create the Map File
##

maptemplate map=$projection.map $mapopt


##
## Latitude of True Scale is Not Supported by the Map Template, So We Add It Manually
##

if [ $projection = "SimpleCylindrical" ]
then
    sed "/CenterLongitude/ i\  TrueScaleLatitude  = $slat" $projection.map > $projection.map.tmp
    mv $projection.map.tmp $projection.map
fi


##
## Check the Map File Exists Before Continuing 
##

if [ ! -e $PWD/$projection.map ] 
then
    echo "Unable to create the map projection parameter file"
    exit;
fi


###############################################################################
###############################################################################
##
## Miscellaneous Checks Before Projecting
##
###############################################################################
###############################################################################

##
## Latitude/Longitude Range Buffer Setup (used by thmcrop.sh)
##

if [ $maxlat != '--' ] 
then
    latrange=`echo "${minlat}:${maxlat}"`
    minlatbuff=`echo ${minlat}-1 | bc -l`
    maxlatbuff=`echo ${maxlat}+1 | bc -l`
fi


##
## Do NOT Perform Destreak if We Perform Night_Clean
##

if [ $night_clean="1" ] 
then
    arc_wnr="3"
fi


##
## Remind User if They are Forcing the Recreation of All Products
##

if [ $force = 1 ]
then 
    echo Forcing the recreation of all products
fi


##
## Check Entries in the File List
##

for i in `cat file_list`
do

  ## Check If the File is NOT Commented Out
  if [ `basename $i | cut -c 1` != "I" ]
  then
      continue;
  fi

  ## Check If the File Already Exists
  if [[ ! -f $PWD/${i}.cub || $force != 0 ]]
  then
      uname=`whoami`
      random=`bash -c 'echo $RANDOM'`
      name=`basename $PWD`_${uname}_${random}_${i}
      name=`echo $name | cut -c 1-35`
      dir="/tmp/${name}/"


###############################################################################
###############################################################################
##
## Create Projection Job for Cluster Submission
##
###############################################################################
###############################################################################

cat > /tmp/$i.$$ <<EOF
#!/bin/bash
source /themis/lib/mosaic/setupisis3beta.bashrc

cleanup() {
  echo 
  echo 'Script has received signal'
  echo 'Removing tmp directory: $dir'
  printf 'on cluster node: '
  hostname
  rm -rf ${dir}
  rmdir ${dir}
  exit 1
}

trap cleanup 9 15

echo "Hostname: \`cat \$PBS_NODEFILE\`"


##
## Make the Tmp Directory and CD Into It
##

mkdir -p $dir
cd $dir 


##
## Copy the Image into the Directory and Then Undrift, Dewobble and Untilt
##

/themis/lib/mosaic/uddw_isis3.dv ${i} ${dir}/${i}_uddw.qub
echo UDDW COMPLETE


##
## Run Night_Clean, If It was Selected
##

if [ "$night_clean" == "1" ] 
then 
    /themis/lib/mosaic/deplaid_night_isis3.dv ${dir}/${i}_uddw.qub ${dir}/${i}_uddw_nd.qub 
    mv ${dir}/${i}_uddw_nd.qub ${dir}/${i}_uddw.qub
    echo NIGHT DEPLAID COMPLETE
fi


##
## Convert THEMIS Image into ISIS3 Level 1 Cube
##

#NOTE: The thm2isis step has already been accomplished by the themis_to_isis3 davinci
#      function called in uddw_isis3.dv. This change was made to enable the ISIS3 history
#      object to be properly updated during the processing steps above.

#thm2isis from=${dir}/${i}_uddw.QUB to=${dir}/${i}.lev1.cub -verbose
mv ${dir}/${i}_uddw.qub ${dir}/${i}.lev1.cub


##
## Load the Appropriate Spice Kernels
##

spiceinit from=${dir}/${i}.lev1.cub cknadir=yes -verbose


##
## Crop by Latitude Before Projection, If It Was Selected
##

if [ "$minlat" != "--" ] 
then 
    /themis/lib/mosaic/thmcrop.sh ${dir}/${i}.lev1.cub ${dir}/${i}.lev1b.cub ${minlat}:${maxlat}
    mv ${dir}/${i}.lev1b.cub ${dir}/${i}.lev1.cub
    echo THMCROP COMPLETE
fi


##
## Project the Image Data
##

cam2map from=${dir}/${i}.lev1.cub to=${dir}/${i}.proj.cub map=${PWD}/$projection.map interp=$interp pixres=map -verbose


##
## Run The Selected Version of Autoradcorr and White Noise Removal
##

## Deplaid Without Destreak
if [ "$arc_wnr" == "4" ]
then
  /themis/lib/mosaic/deplaid_arc_isis3.dv ${dir}/${i}.proj.cub ${dir}/${i}.cub
  echo DEPLAID/DESTREAK/ARC COMMPLETE
fi

if [ "$arc_wnr" == "3" ]
then
  /themis/lib/mosaic/deplaid_no_destreak_isis3.dv ${dir}/${i}.proj.cub ${dir}/${i}.cub
  echo DEPLAID COMPLETE
fi

## Deplaid, Autoradcorr and White Noise Removal 2
if [ "$arc_wnr" == "2" ]
then
  /themis/lib/mosaic/deplaid_arc_wnr2_isis3.dv ${dir}/${i}.proj.cub ${dir}/${i}.cub
  echo DEPLAID/ARC/WNR2 COMPLETE
fi

## Deplaid, Autoradcorr and White Noise Removal 1
if [ "$arc_wnr" == "1" ]
then
  /themis/lib/mosaic/deplaid_arc_wnr_isis3.dv ${dir}/${i}.proj.cub ${dir}/${i}.cub
  echo DEPLAID/ARC/WNR COMPLETE
fi

## Deplaid Without Autoradcorr and White Noise Removal
if [ "$arc_wnr" == "0" ]
then
  /themis/lib/mosaic/deplaid_isis3.dv ${dir}/${i}.proj.cub ${dir}/${i}.cub
  echo DEPLAID/DESTREAK COMPLETE
fi


##
## If the Projected Image Exists, Change to the Final Name
##

if [ ! -e "${dir}/${i}.cub" ]
then
  mv ${dir}/${i}.proj.cub ${dir}/${i}.cub 
fi


##
## Pull Out the Requested Band Number, If Specified
## 

if [ "${bandnum}" != "0" ] 
then
  /themis/lib/mosaic/bandselect_isis3.dv ${dir}/${i}.cub ${dir}/${i}.${bandnum}.cub ${bandnum}
  mv ${dir}/${i}.${bandnum}.cub ${dir}/${i}.cub
  echo BAND SELECTION COMPLETE
fi


##
## If the Projected Image Has a File Size, Move the Cube to the Correct Directory
##
      
if [ \`du ${dir}/${i}.cub | awk '{print \$1}'\` != "0" ]
then 
  mv ${dir}/${i}.cub $PWD
  echo FILE MOVE COMPLETE
fi
rm -rf ${dir}
EOF


###############################################################################
###############################################################################
##
## Submit Projection Job to Cluster
##
###############################################################################
###############################################################################


  if [ "$projlocation" == "0" ]
  then
      chmod 777 /tmp/$i.$$
      cat /tmp/$i.$$ | sed 's/cat $PBS_NODEFILE/hostname/g' > /tmp/$i.$$
      /tmp/$i.$$
      rm /tmp/$i.$$
  fi

  if [ "$projlocation" == "1" ]
  then
      name2=${group}_${i}
      name2=`echo $name2 | cut -c 1-15`
      #qsub ${mail} ${clustervar} -N ${name2} -j oe -W umask=002 -q${queuename} /tmp/$i.$$
      /themis/lib/mosaic/qsub_wrap.pl -l walltime=01:00:00 -d 500 -R 60 ${mail} ${clustervar} -N ${name2} -j oe -l walltime=00:60:00 -W umask=002 -q${queuename} /tmp/$i.$$
      rm /tmp/$i.$$
  fi


###############################################################################
###############################################################################
##
## Close Remaining Loops and End Script
##
###############################################################################
###############################################################################

  else
    echo skipping ${i} because it is complete
  fi

done


###############################################################################
################################ End of Script ################################
###############################################################################
