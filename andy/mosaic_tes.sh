#!/bin/bash -f

############################################################################
############################################################################
##
## Options
##
############################################################################
############################################################################

#Cluster Parameters
projlocation=1                          #Projection Processing Location: 0=local,1=MSFF Cluster
group="tesm"                            #Identification for cluster jobs (Max 5 Characters)
queuename="low"                         #Cluster queue priority
mail="-M cluster-noise@mars.asu.edu"    #Direct the output mail (Use "" for none)
clustervar=""                           #cluster -l option (Use "-l nodes=1 -l qos=virtualize" for hpc)
numproc=10                               #Number of processes allowed to run simultaneously (multiples of 5, default=15)

#TES parameters
fld="lambert_alb"                       #Field for mapping

maplatmin=-90.0                         #Min and max lat for total map (upper lat-10)
maplatmax=80.0
maplatinc=10.0

maplonmin=0.0                           #Min and max lon for total map (upper lon-10) USING DEGREES WEST
maplonmax=350.0
maploninc=10.0


lsmin=0.0                               #Minimum solar longitude for mapping (0.0 to 359.9)
lsmax=359.999                           #Maximum solar longitude for mapping (0.0 to 359.9)
marsyr="ALL"                             #Martian year (24 thru 27, or "ALL") Note: my24 starts at ls 103. My 27 ends at lemcas 81
ppd=32                                   #Pixels per degree for map

loadock="0"                             #Set to "1" to use text file with ock list (from albedo_comp3.dv)
ockfilepath="./blah"                    #Path of ock list text file

############################################################################
############################################################################
##
## Setup
##
############################################################################
############################################################################

#Select ock range from mars year input
case $marsyr in
24)
ocklow=1583
ockhigh=7210
;;
25)
ocklow=7100
ockhigh=15650
;;
26)
ocklow=15500
ockhigh=24050
;;
27)
ocklow=23900
ockhigh=26183
;;
"ALL")
ocklow=1583
ockhigh=26183
;;
*)
echo "You have selected an invalid mars year. Plase choose marsyr 24 thru 27."
exit 1
;;
esac


#build ock command
if [ "$loadock" == "1" ]
then
    echo loading ock file
    ockcommand=`cat ${ockfilepath}`
fi

if [ "$loadock" == "0" ]
then
    echo using full ock range ${ocklow} ${ockhigh} from mars year ${marsyr}
    ockcommand=`echo ock ${ocklow} ${ockhigh}`
fi



#Create output folder in user directory. Name is /file_#day_hour_min
uname=`whoami`
time=`date "+%d_%H_%M"`
udir="/u/${uname}/${fld}_${time}/"
mkdir -p ${udir}
echo creating local directory ${udir}


#Write map parameters to user directory info file
cat > ${udir}info <<EOF 
ls: ${lsmin} ${lsmax} 
marsyr: ${marsyr}  
ppd: ${ppd}
ocks: ${ocklow} ${ockhigh}
Loadock: ${loadock}
EOF



###############################################################################
###############################################################################
##
## Begin lat/lon loops and cluster job prep
##
###############################################################################
###############################################################################


#START OF LAT LOOP
for minlat in $(seq ${maplatmin} ${maplatinc} ${maplatmax})
do 

maxlat=$(echo "scale=1; ${minlat}+${maplatinc}" | bc)
minlatabs=$(echo "scale=1; ${minlat}+90" | bc)



#START OF LON LOOP
for minlon in $(seq ${maplonmin} ${maploninc} ${maplonmax})
do

maxlon=$(echo "scale=1; ${minlon}+${maploninc}" | bc)
file=${minlatabs}_${minlon}

#Set up working directory path
random=`bash -c 'echo $RANDOM'`
name="tesmos_${uname}_${random}"
name=`echo $name | cut -c 1-35`
dir="/tmp/${name}/"



#CREATE MAPPING JOB FOR CLUSTER SUBMISSION
cat > /tmp/${minlatabs}_${minlon}.$$ <<EOF
#!/bin/bash


cleanup() {
    echo 
    echo 'Script has received signal'
    echo 'Removing tmp directory: $dir'
    printf 'on cluster node: '
    hostname
    rm -rf ${dir}
    rmdir ${dir}
    exit 1
}

trap cleanup 9 15

echo "Hostname: \`cat \$PBS_NODEFILE\`"

mkdir -p ${dir}
cd ${dir}

echo Submitting map job
echo Will save to temporary location ${dir}${file}

dmap -o ${dir}${file} -fields "${fld}" -select "${ockcommand} heliocentric_lon ${lsmin} ${lsmax} latitude ${minlat} ${maxlat} longitude ${minlon} ${maxlon} incidence 0 85 emission 0 5 quality:atmospheric_opacity_rating 0 0 atm.nadir_opacity[1] -0.03 0.25 atm.nadir_opacity[2] -0.03 0.2 scan_len 1 1 observation_type D D det_mask 7 7 spectral_mask 0 0 quality:bolometer_lamp_anomaly 0 0" -s ${ppd} -r ${maxlat} ${maxlon} ${minlat} ${minlon} -p min -f f -count -v /mars/readonly/tes/mgst/data/mgst_tabs/dataset.lst


#If map has file size, move to user directory
if [ \`du ${dir}${file} | awk '{print \$1}'\` != "0" ]
then 
    mv ${dir}${file} ${udir}${minlatabs}_${minlon} 
    echo FILE MOVE COMPLETE
fi

rm -rf ${dir}

EOF



###############################################################################
###############################################################################
##
## Submit Projection Job to Cluster
##
###############################################################################
###############################################################################

if [ "$projlocation" == "0" ]
then
    chmod 777 /tmp/${minlatabs}_${minlon}.$$
    cat /tmp/${minlatabs}_${minlon}.$$ | sed 's/cat $PBS_NODEFILE/hostname/g' > /tmp/${minlatabs}_${minlon}.$$
    /tmp/${minlatabs}_${minlon}.$$
    rm /tmp/${minlatabs}_${minlon}.$$
fi

if [ "$projlocation" == "1" ]
then
    name2=${group}_${minlatabs}_${minlon}
    name2=`echo $name2 | cut -c 1-15`
    #qsub ${mail} ${clustervar} -N ${name2} -j oe -W umask=002 -q${queuename} /tmp/${minlatabs}_${minlon}.$$
    /themis/lib/mosaic/qsub_wrap.pl -d 15 -R 60 ${mail} ${clustervar} -N ${name2} -j oe -o ${udir} -l walltime=03:00:00 -l gres=${numproc} -W umask=002 -q${queuename} /tmp/${minlatabs}_${minlon}.$$
    rm /tmp/${minlatabs}_${minlon}.$$
fi

#######
#######Close Loops
#######

#END OF LON LOOP
done


#END OF LAT LOOP
done


###############################################################################
################################ End of Script ################################
###############################################################################