#!/usr/bin/env python
#
# So this is a program which will do a bunch of KRC runs and graph the
# results, with varying parameters.
# Its purpose is to see how sensitive the model is to various things,
# and thus which factors in The Process need to be most accurate.
# The parameters we will change (and defaults) are:
# Time of day - 5 am
# Albedo - 0.2
# Tau - 0.3
# Upper layer TI - 200
# Lower layer TI - 2000
# Elevation - 0
# Degree and azimuth of slope - 0 and 0
# 
# Longitude is actually not used at all by KRC.
#
# I think this is complete-ish, except I still don't
# know how to make KRC change lower-layer TI.  Hmmm.

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

import krc
from imageProcessing import *

latitudes = np.linspace(0, 60, num=4)
layers = [1, 5, 10, 15, 20, 25]
colors = ['red', 'orange', 'green', 'blue', 'violet', 'black']

todDefault = 5
albedoDefault = 0.2
tauDefault = 0.3
upperTIDefault = 200
lowerTIDefault = 2000
elevationDefault = 0.0
slopeDefault = 0.0
azimuthDefault = 0.0

albedos = np.linspace(0.1, 0.4, num=5)
taus = np.linspace(0.1, 0.5, num=5)
upperTIs = np.linspace(50, 800, num=5)
lowerTIs = np.linspace(1000, 2000, num=5)
# ooh yah, KRC takes elevation in kilometers...
elevations = np.linspace(-3, 3, num=6)
slopes = np.linspace(0, 20, num=5)
azimuthEast = 90
azimuthWest = 270

def runAlbedo(lat):
    print "Running albedo tests..."
    accm = []
    for albedo in albedos:
        r = [krc.runKRC(lat, 0, elev=elevationDefault, alb=albedo,
                        tau=tauDefault, slope=slopeDefault, azimuth=azimuthDefault,
                        ti=upperTIDefault, layer=lay)
             for lay in layers]
        accm.append(r)
    return accm

def runTau(lat):
    print "Running tau tests..."
    accm = []
    for t in taus:
        r = [krc.runKRC(lat, 0, elev=elevationDefault, alb=albedoDefault,
                        tau=t, slope=slopeDefault, azimuth=azimuthDefault,
                        ti=upperTIDefault, layer=lay)
             for lay in layers]
        accm.append(r)
    return accm

def runUpperTI(lat):
    print "Running upper-layer TI tests..."
    accm = []
    for t in upperTIs:
        r = [krc.runKRC(lat, 0, elev=elevationDefault, alb=albedoDefault,
                        tau=tauDefault, slope=slopeDefault, azimuth=azimuthDefault,
                        ti=t, layer=lay)
             for lay in layers]
        accm.append(r)
    return accm

def runElevation(lat):
    print "Running elevation tests..."
    accm = []
    for e in elevations:
        r = [krc.runKRC(lat, 0, elev=e, alb=albedoDefault,
                        tau=tauDefault, slope=slopeDefault, azimuth=azimuthDefault,
                        ti=upperTIDefault, layer=lay)
             for lay in layers]
        accm.append(r)
    return accm

def runSouthSlope(lat):
    print "Running southward-facing slope tests..."
    accm = []
    for s in slopes:
        r = [krc.runKRC(lat, 0, elev=elevationDefault, alb=albedoDefault,
                        tau=tauDefault, slope=s, azimuth=azimuthDefault,
                        ti=upperTIDefault, layer=lay)
             for lay in layers]
        accm.append(r)
    return accm


def runEastSlope(lat):
    print "Running east-facing slope tests..."
    accm = []
    for s in slopes:
        r = [krc.runKRC(lat, 0, elev=elevationDefault, alb=albedoDefault,
                        tau=tauDefault, slope=s, azimuth=azimuthEast,
                        ti=upperTIDefault, layer=lay)
             for lay in layers]
        accm.append(r)
    return accm


def runWestSlope(lat):
    print "Running west-facing slope tests..."
    accm = []
    for s in slopes:
        r = [krc.runKRC(lat, 0, elev=elevationDefault, alb=albedoDefault,
                        tau=tauDefault, slope=s, azimuth=azimuthWest,
                        ti=upperTIDefault, layer=lay)
             for lay in layers]
        accm.append(r)
    return accm

def writeGraph(runs, labels, title, filename):
    print "Graphing..."
    # So 'runs' is an array of KRC outputs
    fig = plt.figure()
    plt.xlabel("Ls")
    plt.ylabel("Surface temperature (K)")
    plt.title(title)
    #print runs
    lines = []
    for (r, color, lab) in zip(runs, colors, labels):
        # Each run at a different latitude...
        line = 0
        for i in r:
            # Each run at a different parameter variation...
            temps = i['tk']
            # This is an 80x24 array, so we want time of day 4 (5 am)
            tempsAtTime = temps[:,4]
            line = plt.plot(np.linspace(0, 360, 80), tempsAtTime, color, label=str(lab))
        lines.append(line[0])
    plt.legend(lines, map(lambda l: "{0}".format(l.get_label()), lines))
    plt.xlim((0, 360))
    plt.ylim((140, 220))
    #plt.show()
    i = figToImage(fig, (800, 600))
    i.save(filename)

def main():

    for lat in latitudes:
        print "Running at latitude", lat
        r = runAlbedo(lat)
        writeGraph(r, albedos, "Albedo at lat = {0}".format(lat), "albedo-{0:02.0f}.png".format(lat))
        r = runTau(lat)
        writeGraph(r, taus, "Tau at lat = {0}".format(lat), "tau-{0:02.0f}.png".format(lat))
        r = runUpperTI(lat)
        writeGraph(r, upperTIs, "Upper-layer TI at lat = {0}".format(lat), "ti-upper.png-{0:02.0f}.png".format(lat))
        r = runElevation(lat)
        writeGraph(r, elevations, "Elevations at lat = {0}".format(lat), "elevations.png-{0:02.0f}.png".format(lat))
        r = runSouthSlope(lat)
        writeGraph(r, slopes, "South-facing slope at lat = {0}".format(lat), "slope-south.png-{0:02.0f}.png".format(lat))
        r = runEastSlope(lat)
        writeGraph(r, slopes, "East-facing slope at lat = {0}".format(lat), "slope-east.png-{0:02.0f}.png".format(lat))
        r = runWestSlope(lat)
        writeGraph(r, slopes, "West-facing slope at lat = {0}".format(lat), "slope-west.png-{0:02.0f}.png".format(lat))

if __name__ == '__main__':
    main()
